package com.copiaexigo.grocery.users.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.copiaexigo.grocery.users.HomeActivity;
import com.copiaexigo.grocery.users.R;
import com.copiaexigo.grocery.users.activities.HotelViewActivity;
import com.copiaexigo.grocery.users.activities.LoginActivity;
import com.copiaexigo.grocery.users.activities.ProductFullDetailActivity;
import com.copiaexigo.grocery.users.activities.ViewCartActivity;
import com.copiaexigo.grocery.users.build.api.ApiClient;
import com.copiaexigo.grocery.users.build.api.ApiInterface;
import com.copiaexigo.grocery.users.fragments.CartChoiceModeFragment;
import com.copiaexigo.grocery.users.helper.GlobalData;
import com.copiaexigo.grocery.users.models.AddCart;
import com.copiaexigo.grocery.users.models.Cart;
import com.copiaexigo.grocery.users.models.CartAddon;
import com.copiaexigo.grocery.users.models.ClearCart;
import com.copiaexigo.grocery.users.models.Shop;
import com.copiaexigo.grocery.users.models.specifiedshop.ProductItem;
import com.copiaexigo.grocery.users.utils.Utils;
import com.copiaexigo.grocery.users.utils.roundimage.RoundedImageView;
import com.google.android.gms.common.util.CollectionUtils;
import com.robinhood.ticker.TickerUtils;
import com.robinhood.ticker.TickerView;

import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.copiaexigo.grocery.users.activities.HotelViewActivity.viewCartLayout;
import static com.copiaexigo.grocery.users.helper.GlobalData.notificationCount;
import static com.copiaexigo.grocery.users.helper.GlobalData.profileModel;
import static com.copiaexigo.grocery.users.helper.GlobalData.selectedShop;
import static com.copiaexigo.grocery.users.helper.GlobalData.selectedShopId;

/*
 * Created by santhosh@appoets.com on 22-08-2017.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder> {

    //Animation number
    private static final char[] NUMBER_LIST = TickerUtils.getDefaultNumberList();
    public static AddCart addCart;
    public static int itemQuantity = 0;
    public static ApiInterface apiInterface = ApiClient.getRetrofit().create(ApiInterface.class);
    public static boolean isShopIsChanged = true;
    public static CartChoiceModeFragment bottomSheetDialogFragment;
    private AnimatedVectorDrawableCompat avdProgress;
    private Double priceAmount = 0.0;
    private int itemCount = 0;
    private MyViewHolder holder;
    private Animation slide_down;
    private Animation slide_up;
    private final List<ProductItem> list;
    private final Context context;
    private final int layoutItem;
    private final NumberFormat numberFormat = GlobalData.getNumberFormat();

    public ProductAdapter(List<ProductItem> list, Context con, int layoutItem) {
        this.list = list;
        this.context = con;
        this.layoutItem = layoutItem;
        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(layoutItem, parent, false);
        slide_down = AnimationUtils.loadAnimation(context, R.anim.slide_down);
        slide_up = AnimationUtils.loadAnimation(context, R.anim.slide_up);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ProductItem dish = list.get(position);
        ProductItem product = list.get(position);
        this.holder = holder;

        holder.cardTextValueTicker.setCharacterList(NUMBER_LIST);
        holder.dishNameTxt.setText(product.getName());
        holder.priceTxt.setText(numberFormat.format(product.getPrices().getPrice()));

        if (Objects.equals(product.getPrices().getOrignalPrice(), product.getPrices().getPrice())) {
            try {
                holder.saveAmountTxt.setVisibility(View.GONE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            double value = product.getPrices().getPrice() - product.getPrices().getOrignalPrice();
            if (holder.saveAmountTxt != null)
                holder.saveAmountTxt.setText("Save " + numberFormat.format(value));
        }

        if (!CollectionUtils.isEmpty(product.getImages())) {
            Glide.with(context)
                    .load(product.getImages().get(0).getUrl())
                    .apply(new RequestOptions()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.ic_leader_board)
                            .error(R.drawable.ic_leader_board))
                    .into(holder.featured_image);
        }

        addCart = GlobalData.addCart;
        List<CartAddon> cartAddonList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(product.getCart())) {
            selectedShop = HotelViewActivity.shops;
            holder.cardAddTextLayout.setVisibility(View.GONE);
            holder.cardAddDetailLayout.setVisibility(View.VISIBLE);
            Integer quantity = 0;
            for (Cart cart : product.getCart()) {
                quantity += cart.getQuantity();
                cartAddonList = cart.getCartAddons();
            }
            holder.cardTextValueTicker.setText(String.valueOf(quantity));
            holder.cardTextValue.setText(String.valueOf(quantity));
        } else {
            holder.cardAddTextLayout.setVisibility(View.VISIBLE);
            holder.cardAddDetailLayout.setVisibility(View.GONE);
            holder.cardTextValueTicker.setText(String.valueOf(1));
            holder.cardTextValue.setText(String.valueOf(1));
        }

        if (product.getAddons() != null && product.getAddons().size() != 0) {
            //holder.customizableTxt.setVisibility(View.VISIBLE);
            holder.addOnsIconImg.setVisibility(View.VISIBLE);
        } else {
            //holder.customizableTxt.setVisibility(View.GONE);
            holder.addOnsIconImg.setVisibility(View.GONE);
        }

        holder.cardAddBtn.setOnClickListener(v -> {
            Log.e("access_token2", GlobalData.accessToken);
            if (!shopClosedStatus() && !isCategory()) {
                Toast.makeText(context, "Shop Closed", Toast.LENGTH_SHORT).show();
                return;
            }
            /* Press Add Card Add button */
            ProductItem mProduct = list.get(position);
            selectedShopId = mProduct.getShopId();
            if (mProduct.getAddons() != null && !mProduct.getAddons().isEmpty()) {
                openProductFullDetails(mProduct.getId());
            } else {
                int cartId = 0;
                if (!CollectionUtils.isEmpty(mProduct.getCart())) {
                    for (int i = 0; i < mProduct.getCart().size(); i++) {
                        cartId = mProduct.getCart().get(i).getId();
                    }
                }
                int countValue = Integer.parseInt(holder.cardTextValue.getText().toString()) + 1;
                HashMap<String, String> map = new HashMap<>();
                map.put("product_id", product.getId().toString());
                map.put("quantity", String.valueOf(countValue));
                map.put("cart_id", String.valueOf(cartId));
                Log.e("AddCart_add", map.toString());
                addCart(map, countValue, mProduct, holder);
            }
        });

        holder.cardMinusBtn.setOnClickListener(v -> {
            /* Press Add Card Minus button */
            if (!shopClosedStatus() && !isCategory()) {
                Toast.makeText(context, "Shop Closed", Toast.LENGTH_SHORT).show();
                return;
            }
            ProductItem mProduct = list.get(position);
            selectedShopId = mProduct.getShopId();
            int cartId = 0;
            if (!CollectionUtils.isEmpty(addCart.getProductList())) {
                for (int i = 0; i < addCart.getProductList().size(); i++) {
                    if (addCart.getProductList().get(i).getProductId().equals(product.getId())) {
                        cartId = addCart.getProductList().get(i).getId();
                    }
                }
            }
            if (holder.cardTextValue.getText().toString().equalsIgnoreCase("1")) {
                int countMinusValue = Integer.parseInt(holder.cardTextValue.getText().toString()) - 1;
                holder.cardTextValue.setText("" + countMinusValue);
                holder.cardTextValueTicker.setText("" + countMinusValue);
                holder.cardAddDetailLayout.setVisibility(View.GONE);
                if (addCart.getProductList().size() == 0 && addCart != null)
                    viewCartLayout.setVisibility(View.GONE);
                holder.cardAddTextLayout.setVisibility(View.VISIBLE);
                HashMap<String, String> map = new HashMap<>();
                map.put("product_id", product.getId().toString());
                map.put("quantity", "0");
                map.put("cart_id", String.valueOf(cartId));
                Log.e("AddCart_Minus", map.toString());
                addCart(map, countMinusValue, mProduct, holder);
            } else {
                if (!CollectionUtils.isEmpty(mProduct.getCart()) || mProduct.getCart().size() == 1) {
                    int countMinusValue = Integer.parseInt(holder.cardTextValue.getText().toString()) - 1;
                    HashMap<String, String> map = new HashMap<>();
                    map.put("product_id", product.getId().toString());
                    map.put("quantity", String.valueOf(countMinusValue));
                    map.put("cart_id", String.valueOf(cartId));
                    Log.e("AddCart_Minus", map.toString());
                    addCart(map, countMinusValue, mProduct, holder);
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle(context.getResources().getString(R.string.remove_item_from_cart))
                            .setMessage(context.getResources().getString(R.string.remove_item_from_cart_description))
                            .setPositiveButton(context.getResources().getString(R.string.yes), (dialog, which) -> {
                                // continue with delete
                                context.startActivity(new Intent(context, ViewCartActivity.class));
                                ((Activity) context).overridePendingTransition(R.anim.slide_in_right, R.anim.anim_nothing);

                            })
                            .setNegativeButton(context.getResources().getString(R.string.no), (dialog, which) -> {
                                // do nothing
                                dialog.dismiss();

                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                    Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
                    nbutton.setTextColor(ContextCompat.getColor(context, R.color.theme));
                    nbutton.setTypeface(nbutton.getTypeface(), Typeface.BOLD);
                    Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
                    pbutton.setTextColor(ContextCompat.getColor(context, R.color.theme));
                    pbutton.setTypeface(pbutton.getTypeface(), Typeface.BOLD);
                }
            }
        });

        holder.rootLayout.setOnClickListener(view -> {
            openProductFullDetails(product.getId());
        });
        /* check Availablity*/
/*        if (product.getAvaialable().equalsIgnoreCase("available")) {
            holder.cardAddTextLayout.setVisibility(View.VISIBLE);
            holder.cardTextValueTicker.setText(String.valueOf(1));
            holder.cardInfoLayout.setVisibility(View.GONE);
        } else if (product.getAvaialable().equalsIgnoreCase("out of stock")) {
            holder.cardAddTextLayout.setVisibility(View.GONE);
            holder.cardInfoLayout.setVisibility(View.VISIBLE);
            holder.cardAddInfoText.setVisibility(View.GONE);
            holder.cardAddOutOfStock.setVisibility(View.VISIBLE);
        } else {
            holder.cardAddTextLayout.setVisibility(View.GONE);
            holder.cardInfoLayout.setVisibility(View.VISIBLE);
            holder.cardAddInfoText.setVisibility(View.VISIBLE);
            holder.cardAddOutOfStock.setVisibility(View.GONE);
            holder.cardAddInfoText.setText(product.getAvaialable());
        }*/

/*        if (product.getIsVeg()) {
            holder.foodImageType.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_nonveg));
        } else {
            holder.foodImageType.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_veg));
        }*/


        holder.cardAddTextLayout.setOnClickListener(v -> {
            /* Press Add Card Text Layout */
            if (!shopClosedStatus() && !isCategory()) {
                Toast.makeText(context, "Shop Closed", Toast.LENGTH_SHORT).show();
                return;
            }
            ProductItem mProduct = list.get(position);
            selectedShopId = list.get(position).getShopId();
            if (profileModel != null) {
                if (Utils.isShopChanged(mProduct.getShopId())) {
                    String message = String.format(context.getResources().getString(R.string.reorder_confirm_message), mProduct.getName(),
                            GlobalData.addCart.getProductList().get(0).getProduct().getShop().getName());
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle(context.getResources().getString(R.string.replace_cart_item))
                            .setMessage(message)
                            .setPositiveButton(context.getResources().getString(R.string.yes), (dialog, which) -> {
                                // continue with delete
                                clearCart();
                                isShopIsChanged = false;
                                if (mProduct.getAddons() != null && mProduct.getAddons().size() != 0) {
                                    GlobalData.isSelectedProductItem = mProduct;
                                    openProductFullDetails(GlobalData.isSelectedProductItem.getId());
                                } else {
                                    //selectedShop = product.getShop();
                                    holder.cardAddDetailLayout.setVisibility(View.VISIBLE);
                                    holder.cardAddTextLayout.setVisibility(View.GONE);
                                    HashMap<String, String> map = new HashMap<>();
                                    map.put("product_id", mProduct.getId().toString());
                                    map.put("quantity", "1");
                                    Log.e("AddCart_Text", map.toString());
                                    addCart(map, 1, mProduct, holder);
                                }

                            }).setNegativeButton(context.getResources().getString(R.string.no), (dialog, which) -> {
                        // do nothing
                        dialog.dismiss();

                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                    Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
                    nbutton.setTextColor(ContextCompat.getColor(context, R.color.theme));
                    nbutton.setTypeface(nbutton.getTypeface(), Typeface.BOLD);
                    Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
                    pbutton.setTextColor(ContextCompat.getColor(context, R.color.theme));
                    pbutton.setTypeface(pbutton.getTypeface(), Typeface.BOLD);
                } else {
                    if (product.getAddons() != null && product.getAddons().size() != 0) {
                        GlobalData.isSelectedProductItem = product;
                        openProductFullDetails(GlobalData.isSelectedProductItem.getId());
                    } else {
                        holder.cardAddDetailLayout.setVisibility(View.VISIBLE);
                        holder.cardAddTextLayout.setVisibility(View.GONE);
                        HashMap<String, String> map = new HashMap<>();
                        map.put("product_id", product.getId().toString());
                        map.put("quantity", holder.cardTextValue.getText().toString());
                        Log.e("AddCart_Text", map.toString());
                        addCart(map, 1, mProduct, holder);
                    }
                }
            } else {
                context.startActivity(new Intent(context, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                ((Activity) context).overridePendingTransition(R.anim.slide_in_left, R.anim.anim_nothing);
                ((Activity) context).finish();
                Toast.makeText(context, context.getResources().getString(R.string.please_login_and_order_dishes), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void openProductFullDetails(Integer itemId) {
        Intent intent = new Intent(context, ProductFullDetailActivity.class);
        intent.putExtra("productItemId", itemId);
        context.startActivity(intent);
        ((Activity) context).overridePendingTransition(R.anim.slide_in_right, R.anim.anim_nothing);
    }

    private void clearCart() {
        Call<ClearCart> call = apiInterface.clearCart();
        call.enqueue(new Callback<ClearCart>() {
            @Override
            public void onResponse(Call<ClearCart> call, Response<ClearCart> response) {
                if (response != null && !response.isSuccessful() && response.errorBody() != null) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(context, jObjError.optString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else if (response.isSuccessful()) {
                    selectedShop = HotelViewActivity.shops;
                    GlobalData.addCart.getProductList().clear();
                    GlobalData.notificationCount = 0;
                    if (GlobalData.addCart != null && GlobalData.addCart.getProductList().size() != 0) {
                        viewCartLayout.setVisibility(View.VISIBLE);
                    } else {
                        viewCartLayout.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ClearCart> call, Throwable t) {
                Toast.makeText(context, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                GlobalData.addCartShopId = selectedShop.getId();
            }
        });

    }

    public void addCart(HashMap<String, String> map, int countValue, ProductItem mProduct, MyViewHolder holder) {
        Call<AddCart> call = apiInterface.postAddCart(map);
        call.enqueue(new Callback<AddCart>() {
            @Override
            public void onResponse(Call<AddCart> call, Response<AddCart> response) {
                GlobalData.selectedShopId = mProduct.getShopId();
                holder.cardTextValue.setText("" + countValue);
                holder.cardTextValueTicker.setText("" + countValue);
                if (response != null && !response.isSuccessful() && response.errorBody() != null) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(context, jObjError.optString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else if (response.isSuccessful()) {
                    if (selectedShop != null) {
                        GlobalData.addCartShopId = selectedShop.getId();
                        addCart = response.body();
                        GlobalData.addCart = response.body();

                        priceAmount = 0.0;
                        itemQuantity = 0;
                        itemCount = 0;
                        //get Item Count
                        itemCount = addCart.getProductList().size();
                        for (int i = 0; i < itemCount; i++) {
                            //Get Total item Quantity
                            itemQuantity = itemQuantity + addCart.getProductList().get(i).getQuantity();
                            //Get addon price
                            if (addCart.getProductList().get(i).getProduct().getPrices().getPrice() != null)
                                priceAmount = priceAmount + (addCart.getProductList().get(i).getQuantity() * addCart.getProductList().get(i).getProduct().getPrices().getPrice());
                        }
                        notificationCount = itemQuantity;
                        HomeActivity.updateNotificationCount(context, notificationCount);
                        if (GlobalData.addCart != null && GlobalData.addCart.getProductList().size() != 0) {
                            viewCartLayout.setVisibility(View.VISIBLE);
                            ((HotelViewActivity) context).setViewcartBottomLayout(GlobalData.addCart);
                        } else {
                            viewCartLayout.setVisibility(View.GONE);
                        }
                    }
                }
                GlobalData.addCart = response.body();
                if (GlobalData.addCart != null && GlobalData.addCart.getProductList().size() != 0) {
                    viewCartLayout.setVisibility(View.VISIBLE);
                    ((HotelViewActivity) context).setViewcartBottomLayout(GlobalData.addCart);
                } else {
                    viewCartLayout.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<AddCart> call, Throwable t) {

            }
        });

    }

    private void setViewcartBottomLayout(AddCart addCart) {


        //get Item Count
        itemCount = addCart.getProductList().size();
        for (int i = 0; i < itemCount; i++) {
            //Get Total item Quantity
            itemQuantity = itemQuantity + addCart.getProductList().get(i).getQuantity();
            //Get product price
            if (addCart.getProductList().get(i).getProduct().getPrices() != null)
                priceAmount = priceAmount + (addCart.getProductList().get(i).getQuantity() * addCart.getProductList().get(i).getProduct().getPrices().getOrignalPrice());
            if (addCart.getProductList().get(i).getCartAddons() != null && !addCart.getProductList().get(i).getCartAddons().isEmpty()) {
                for (int j = 0; j < addCart.getProductList().get(i).getCartAddons().size(); j++) {
                    if (addCart.getProductList().get(i).getCartAddons().get(j).getAddonProduct() != null) {
                        priceAmount = priceAmount + (addCart.getProductList().get(i).getQuantity()
                                * (addCart.getProductList().get(i).getCartAddons().get(j).getQuantity()
                                * addCart.getProductList().get(i).getCartAddons().get(j).getAddonProduct().getPrice()));
                    }
                }
            }
        }
        GlobalData.notificationCount = itemQuantity;
        if (itemQuantity == 0) {
            viewCartLayout.setVisibility(View.GONE);
            // Start animation
            viewCartLayout.startAnimation(slide_down);
        } else if (itemQuantity == 1) {
            HotelViewActivity.itemText.setText(itemQuantity + " Item | " + numberFormat.format(priceAmount));
            if (!isCategory()) {
                if (!selectedShop.getId().equals(GlobalData.addCart.getProductList().get(0).getProduct().getShopId())) {
                    HotelViewActivity.viewCartShopName.setText("From : " + GlobalData.addCart.getProductList().get(0).getProduct().getShop().getName());
                } else
                    HotelViewActivity.viewCartShopName.setVisibility(View.GONE);
            } else {
                HotelViewActivity.viewCartShopName.setText("From : " + GlobalData.addCart.getProductList().get(0).getProduct().getShop().getName());
            }
            if (viewCartLayout.getVisibility() == View.GONE) {
                // Start animation
                viewCartLayout.setVisibility(View.VISIBLE);
                viewCartLayout.startAnimation(slide_up);
            }
        } else {
            HotelViewActivity.itemText.setText(itemQuantity + " Item | " + numberFormat.format(priceAmount));
            HotelViewActivity.viewCartShopName.setVisibility(View.VISIBLE);
            if (!isCategory()) {
                if (!selectedShop.getId().equals(GlobalData.addCart.getProductList().get(0).getProduct().getShopId())) {
                    HotelViewActivity.viewCartShopName.setText("From : " + GlobalData.addCart.getProductList().get(0).getProduct().getShop().getName());
                } else
                    HotelViewActivity.viewCartShopName.setVisibility(View.GONE);
            } else {
                HotelViewActivity.viewCartShopName.setText("From : " + GlobalData.addCart.getProductList().get(0).getProduct().getShop().getName());
            }
            if (viewCartLayout.getVisibility() == View.GONE) {
                // Start animation
                viewCartLayout.setVisibility(View.VISIBLE);
                viewCartLayout.startAnimation(slide_up);
            }
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ProductItem product;
        public ApiInterface apiInterface = ApiClient.getRetrofit().create(ApiInterface.class);
        public double priceAmount = 0;
        public int itemCount = 0;
        public int itemQuantity = 0;
        public AddCart addCart;
        public final Shop currentShop = new Shop();
        private final TickerView cardTextValueTicker;
        private final LinearLayout rootLayout;
        private final RelativeLayout cardAddDetailLayout;
        private final RelativeLayout cardAddTextLayout;
        private final RelativeLayout cardInfoLayout;
        private final ImageView dishImg;
        private final ImageView foodImageType;
        private final ImageView addOnsIconImg;
        private final ImageView cardAddBtn;
        private final ImageView cardMinusBtn;
        private final ImageView add_item;
        private final ImageView animationLineCartAdd;
        private final RoundedImageView featured_image;
        private final TextView dishNameTxt;
        private final TextView priceTxt;
        private final TextView cardTextValue;
        private final TextView customizableTxt;
        private final TextView cardAddInfoText;
        private final TextView cardAddOutOfStock;
        private final TextView saveAmountTxt;

        private MyViewHolder(View view) {
            super(view);
            dishImg = view.findViewById(R.id.dishImg);
            foodImageType = view.findViewById(R.id.food_type_image);
            animationLineCartAdd = view.findViewById(R.id.animation_line_cart_add);
            dishNameTxt = view.findViewById(R.id.dish_name_text);
            priceTxt = view.findViewById(R.id.price_text);
            rootLayout = view.findViewById(R.id.root_layout);

            /*    Add card Button Layout*/
            cardAddDetailLayout = view.findViewById(R.id.add_card_layout);
            featured_image = view.findViewById(R.id.featured_image);
            cardAddTextLayout = view.findViewById(R.id.add_card_text_layout);
            cardInfoLayout = view.findViewById(R.id.add_card_info_layout);
            cardAddInfoText = view.findViewById(R.id.avialablity_time);
            cardAddOutOfStock = view.findViewById(R.id.out_of_stock);
            cardAddBtn = view.findViewById(R.id.card_add_btn);
            cardMinusBtn = view.findViewById(R.id.card_minus_btn);
            cardTextValue = view.findViewById(R.id.card_value);
            cardTextValueTicker = view.findViewById(R.id.card_value_ticker);
            addOnsIconImg = itemView.findViewById(R.id.add_ons_icon);

            customizableTxt = itemView.findViewById(R.id.customizable_txt);
            saveAmountTxt = itemView.findViewById(R.id.save_amount_txt);
            //itemView.setOnClickListener( this);
            add_item = view.findViewById(R.id.add_item);

            /*  Click Events*/
            cardAddTextLayout.setOnClickListener(this);
            cardAddBtn.setOnClickListener(this);
            cardMinusBtn.setOnClickListener(this);
        }

        public void addCart(HashMap<String, String> map) {
            Call<AddCart> call = apiInterface.postAddCart(map);
            call.enqueue(new Callback<AddCart>() {
                @Override
                public void onResponse(Call<AddCart> call, Response<AddCart> response) {

                    if (response != null && !response.isSuccessful() && response.errorBody() != null) {
                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            Toast.makeText(context, jObjError.optString("message"), Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    } else if (response.isSuccessful()) {
                        GlobalData.addCartShopId = selectedShopId;
                        addCart = response.body();
                        GlobalData.addCart = response.body();

                        priceAmount = 0;
                        itemQuantity = 0;
                        itemCount = 0;
                        //get Item Count
                        itemCount = addCart.getProductList().size();
                        for (int i = 0; i < itemCount; i++) {
                            //Get Total item Quantity
                            itemQuantity = itemQuantity + addCart.getProductList().get(i).getQuantity();
                            //Get addon price
                            if (addCart.getProductList().get(i).getProduct().getPrices().getPrice() != null)
                                priceAmount = priceAmount + (addCart.getProductList().get(i).getQuantity() * addCart.getProductList().get(i).getProduct().getPrices().getPrice());
                        }

                        HotelViewActivity.itemText.setText(itemCount + " Item | " + numberFormat.format(priceAmount));
                        notificationCount = itemQuantity;
                        HomeActivity.updateNotificationCount(context, notificationCount);
                    }
                }

                @Override
                public void onFailure(Call<AddCart> call, Throwable t) {

                }
            });

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.add_card_text_layout:
                    cardAddDetailLayout.setVisibility(View.VISIBLE);
                    viewCartLayout.setVisibility(View.VISIBLE);
                    itemCount = itemCount + 1;
                    priceAmount = priceAmount + list.get(getAdapterPosition()).getPrices().getPrice();
                    HotelViewActivity.itemText.setText(itemCount + " Item | " + numberFormat.format(priceAmount));
                    cardAddTextLayout.setVisibility(View.GONE);
                    break;

                case R.id.card_add_btn:
                    int countValue = Integer.parseInt(cardTextValue.getText().toString()) + 1;
                    itemCount = itemCount + 1;
                    priceAmount = priceAmount + list.get(getAdapterPosition()).getPrices().getPrice();
                    HotelViewActivity.itemText.setText(itemCount + " Items | " + numberFormat.format(priceAmount));
                    cardTextValue.setText(countValue);
                    cardTextValueTicker.setText(String.valueOf(countValue));
                    break;
                case R.id.card_minus_btn:
                    if (cardTextValue.getText().toString().equalsIgnoreCase("1")) {
                        cardAddDetailLayout.setVisibility(View.GONE);
                        viewCartLayout.setVisibility(View.GONE);
                        cardAddTextLayout.setVisibility(View.VISIBLE);
                    } else {
                        int countMinusValue = Integer.parseInt(cardTextValue.getText().toString()) - 1;
                        itemCount = itemCount - 1;
                        priceAmount = priceAmount - list.get(getAdapterPosition()).getPrices().getPrice();
                        HotelViewActivity.itemText.setText(itemCount + " Items | " + numberFormat.format(priceAmount));
                        cardTextValue.setText(countMinusValue);
                        cardTextValueTicker.setText(String.valueOf(countMinusValue));
                    }
                    break;
            }
        }
    }

    private boolean shopClosedStatus() {
        if (selectedShop != null && selectedShop.getShopstatus() != null) {
            return !selectedShop.getShopstatus().equalsIgnoreCase("CLOSED");
        }
        return false;
    }

    boolean isCategory = false;

    public boolean isCategory() {
        return isCategory;
    }

    public void setCategory(boolean category) {
        isCategory = category;
    }

    private int cartId = 0;

    private int getCartId() {
        return cartId;
    }
}
