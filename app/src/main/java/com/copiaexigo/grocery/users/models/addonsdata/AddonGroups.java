
package com.copiaexigo.grocery.users.models.addonsdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class AddonGroups implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("shop_id")
    @Expose
    private Integer shopId;
    @SerializedName("min_selection")
    @Expose
    private Integer minSelection;
    @SerializedName("max_selection")
    @Expose
    private Integer maxSelection;
    @SerializedName("addon")
    @Expose
    private List<AddonsList> addon = null;



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getMinSelection() {
        return minSelection;
    }

    public void setMinSelection(Integer minSelection) {
        this.minSelection = minSelection;
    }

    public Integer getMaxSelection() {
        return maxSelection;
    }

    public void setMaxSelection(Integer maxSelection) {
        this.maxSelection = maxSelection;
    }

    public List<AddonsList> getAddon() {
        return addon;
    }

    public void setAddon(List<AddonsList> addon) {
        this.addon = addon;
    }


}
