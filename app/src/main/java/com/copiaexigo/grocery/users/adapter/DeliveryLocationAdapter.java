package com.copiaexigo.grocery.users.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.copiaexigo.grocery.users.R;
import com.copiaexigo.grocery.users.activities.SetDeliveryLocationActivity;
import com.copiaexigo.grocery.users.helper.GlobalData;
import com.copiaexigo.grocery.users.models.Address;
import com.copiaexigo.grocery.users.models.AddressList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by santhosh@appoets.com on 28-08-2017.
 */

public class DeliveryLocationAdapter extends SectionedRecyclerViewAdapter<DeliveryLocationAdapter.ViewHolder> {

    private List<AddressList> list = new ArrayList<>();
    private final LayoutInflater inflater;
    private final Context context;
    private final Activity activity;

    public DeliveryLocationAdapter(Context context, Activity activity, List<AddressList> list) {
        this.context = context;
        this.activity = activity;
        this.inflater = LayoutInflater.from(context);
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        switch (viewType) {
            case VIEW_TYPE_HEADER:
                v = inflater.inflate(R.layout.header, parent, false);
                return new ViewHolder(v, true);
            case VIEW_TYPE_ITEM:
                v = inflater.inflate(R.layout.location_list_item, parent, false);
                return new ViewHolder(v, false);
            default:
                v = inflater.inflate(R.layout.location_list_item, parent, false);
                return new ViewHolder(v, false);
        }
    }

    @Override
    public int getSectionCount() {
        return list.size();
    }

    @Override
    public int getItemCount(int section) {
        return list.get(section).getAddresses().size();
    }

    @Override
    public void onBindHeaderViewHolder(ViewHolder holder, final int section) {
        holder.path.setText(list.get(section).getHeader());
        holder.path.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println(list.get(section).getHeader());
            }
        });
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int section, int relativePosition, int absolutePosition) {
        final Address object = list.get(section).getAddresses().get(relativePosition);
        holder.addressLabel.setText(object.getType());
        holder.address.setText(object.getMapAddress());
        setIcon(holder.icon, object.getType());
        holder.itemLayout.setOnClickListener(v -> {
            if (SetDeliveryLocationActivity.isAddressSelection) {
                //select the address data and set to address in Cart fargment page
                Intent returnIntent = new Intent();
                GlobalData.selectedAddress = object;
                activity.setResult(Activity.RESULT_OK, returnIntent);
                activity.finish();
            }
        });
    }

    private void setIcon(ImageView imgView, String id) {
        switch (id) {
            case "home":
                imgView.setImageResource(R.drawable.home);
                break;
            case "work":
                imgView.setImageResource(R.drawable.ic_work);
                break;
            default:
                imgView.setImageResource(R.drawable.location_others);
                break;
        }
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView path;
        private TextView addressLabel;
        private TextView address;
        private ImageView icon;
        private LinearLayout itemLayout;

        public ViewHolder(View itemView, boolean isHeader) {
            super(itemView);
            if (isHeader) {
                path = itemView.findViewById(R.id.header);
            } else {
                itemLayout = itemView.findViewById(R.id.item_layout);
                addressLabel = itemView.findViewById(R.id.address_label);
                address = itemView.findViewById(R.id.address);
                icon = itemView.findViewById(R.id.icon);
            }
        }
    }
}
