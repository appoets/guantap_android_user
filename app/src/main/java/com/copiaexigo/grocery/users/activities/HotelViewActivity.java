package com.copiaexigo.grocery.users.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.NestedScrollView;
import androidx.palette.graphics.Palette;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.copiaexigo.grocery.users.HeaderView;
import com.copiaexigo.grocery.users.R;
import com.copiaexigo.grocery.users.adapter.HotelCatagoeryAdapter;
import com.copiaexigo.grocery.users.adapter.ProductAdapter;
import com.copiaexigo.grocery.users.build.api.ApiClient;
import com.copiaexigo.grocery.users.build.api.ApiInterface;
import com.copiaexigo.grocery.users.helper.ConnectionHelper;
import com.copiaexigo.grocery.users.helper.GlobalData;
import com.copiaexigo.grocery.users.helper.SharedHelper;
import com.copiaexigo.grocery.users.models.AddCart;
import com.copiaexigo.grocery.users.models.AddressList;
import com.copiaexigo.grocery.users.models.Category;
import com.copiaexigo.grocery.users.models.CategoryPackages;
import com.copiaexigo.grocery.users.models.Favorite;
import com.copiaexigo.grocery.users.models.Product;
import com.copiaexigo.grocery.users.models.Shop;
import com.copiaexigo.grocery.users.models.User;
import com.copiaexigo.grocery.users.models.specifiedshop.ProductItem;
import com.copiaexigo.grocery.users.models.specifiedshop.SpecifiedShopResponse;
import com.copiaexigo.grocery.users.utils.Utils;
import com.ethanhua.skeleton.Skeleton;
import com.ethanhua.skeleton.ViewSkeletonScreen;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.sackcentury.shinebuttonlib.ShineButton;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.copiaexigo.grocery.users.adapter.HotelCatagoeryAdapter.bottomSheetDialogFragment;
import static com.copiaexigo.grocery.users.helper.GlobalData.addCart;
import static com.copiaexigo.grocery.users.helper.GlobalData.latitude;
import static com.copiaexigo.grocery.users.helper.GlobalData.longitude;

//import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class HotelViewActivity extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener {

    private static final String TAG = "HotelViewActivity";
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recommended_dishes_rv)
    RecyclerView recommendedDishesRv;
    @BindView(R.id.accompaniment_dishes_rv)
    RecyclerView accompanimentDishesRv;
    @BindView(R.id.weekly_savings_)
    RecyclerView weeklyRecyclerView;
    @BindView(R.id.heart_btn)
    ShineButton heartBtn;
    @BindView(R.id.view_line)
    View viewLine;
    @BindView(R.id.llFeaturedProduct)
    LinearLayout llFeaturedProduct;
    @BindView(R.id.llSalesProduct)
    LinearLayout llSalesProduct;
    @BindView(R.id.weekly_savings_layout)
    LinearLayout weekly_savings_layout;
    @BindView(R.id.restaurant_title2)
    TextView restaurantTitle2;
    @BindView(R.id.restaurant_subtitle2)
    TextView restaurantSubtitle2;
    @BindView(R.id.restaurant_image)
    ImageView restaurantImage;
    @BindView(R.id.header_view_title)
    TextView headerViewTitle;
    @BindView(R.id.header_view_sub_title)
    TextView headerViewSubTitle;
    @BindView(R.id.toolLayout)
    RelativeLayout toolLayout;
    @BindView(R.id.resturantname_tv)
    TextView resturantname_tv;
    @BindView(R.id.resturant_descb_tv)
    TextView resturant_descb_tv;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.scroll)
    NestedScrollView scroll;
    @BindView(R.id.offer)
    TextView offer;
    @BindView(R.id.rating)
    TextView rating;
    @BindView(R.id.sales_txtView)
    TextView salesTxtView;
    @BindView(R.id.delivery_time)
    TextView deliveryTime;
    @BindView(R.id.root_layout)
    CoordinatorLayout rootLayout;
    @BindView(R.id.toolbar_header_view)
    HeaderView toolbarHeaderView;
    @BindView(R.id.float_header_view)
    HeaderView floatHeaderView;
    @BindView(R.id.rv_featured_product_)
    RecyclerView rvFeaturedProduct;
    @BindView(R.id.rv_sales_)
    RecyclerView rvSalesProduct;
    boolean isFavourite = false;
    @BindView(R.id.sales_seeall)
    TextView tvSalesSeeAll;
    @BindView(R.id.see_all_featured)
    TextView tvSeeAllFeatured;

    public static TextView itemText;
    public static TextView viewCartShopName;
    public static TextView viewCart;
    public static RelativeLayout viewCartLayout;
    public static Shop shops;
    public static List<Category> categoryList;
    public static List<Product> featureProductList;
    public static HotelCatagoeryAdapter catagoeryAdapter;
    private final ApiInterface apiInterface = ApiClient.getRetrofit().create(ApiInterface.class);
    private final List<ProductItem> featuredProductItems = new ArrayList<>();
    private final List<ProductItem> salesProductItems = new ArrayList<>();

    private Animation slide_down, slide_up;
    private ProductAdapter adapterProduct;
    private ProductAdapter adapterSales;
    private Context context;
    private ConnectionHelper connectionHelper;
    private Activity activity;
    private ViewSkeletonScreen skeleton;
    private boolean isHideToolbarView = false;
    private Boolean isCategory;
    private int categoryPosition;
    private final NumberFormat numberFormat = GlobalData.getNumberFormat();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_view);
        ButterKnife.bind(this);
        context = HotelViewActivity.this;
        activity = HotelViewActivity.this;
        connectionHelper = new ConnectionHelper(context);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        isCategory = getIntent().getBooleanExtra("isCategory", false);
        categoryPosition = getIntent().getIntExtra("categoryPosition", 0);
        String categoryTitle = getIntent().getStringExtra("categoryTitle");
        String categoryImage = getIntent().getStringExtra("categoryImage");

        if (!isCategory) {
            adapterProduct = new ProductAdapter(featuredProductItems, context, R.layout.accompainment_list_item);
            rvFeaturedProduct.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            rvFeaturedProduct.setItemAnimator(new DefaultItemAnimator());
            rvFeaturedProduct.setAdapter(adapterProduct);
        }

        if (isCategory) {
            weekly_savings_layout.setVisibility(View.GONE);
            weeklyRecyclerView.setVisibility(View.GONE);
            llFeaturedProduct.setVisibility(View.GONE);
            salesTxtView.setText(categoryTitle);
            imageLoading(categoryImage);
        }

        adapterSales = new ProductAdapter(salesProductItems, context, R.layout.accompainment_list_item);
        rvSalesProduct.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        rvSalesProduct.setItemAnimator(new DefaultItemAnimator());
        rvSalesProduct.setAdapter(adapterSales);

        appBarLayout.addOnOffsetChangedListener(this);
        categoryList = new ArrayList<>();
        shops = GlobalData.selectedShop;

        viewCart = findViewById(R.id.view_cart);
        viewCartLayout = findViewById(R.id.view_cart_layout);

        itemText = findViewById(R.id.item_text);
        viewCartShopName = findViewById(R.id.view_cart_shop_name);

        viewCartLayout.setOnClickListener(v -> {
            startActivity(new Intent(HotelViewActivity.this, ViewCartActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.anim_nothing);
        });

        skeleton = Skeleton.bind(rootLayout)
                .load(R.layout.skeleton_hotel_view)
                .show();
        slide_down = AnimationUtils.loadAnimation(context, R.anim.slide_down);
        slide_up = AnimationUtils.loadAnimation(context, R.anim.slide_up);
        if (!isCategory) {
            if (shops != null) {
                //Load animation
                isFavourite = getIntent().getBooleanExtra("is_fav", false);
                if (shops.getOfferPercent() == null) {
                    offer.setVisibility(View.GONE);
                } else {
                    if (shops.getOfferPercent() == 0) {
                        offer.setVisibility(View.GONE);
                    } else {
                        offer.setVisibility(View.VISIBLE);
                        offer.setText("Flat " + shops.getOfferPercent().toString() + "% offer on all Orders");
                    }
                }
                if (shops.getRating() != null) {
                    double ratingValue = new BigDecimal(shops.getRating()).setScale(1, RoundingMode.HALF_UP).doubleValue();
                    rating.setText("" + ratingValue);
                } else
                    rating.setText("No Rating");

                deliveryTime.setText(shops.getEstimatedDeliveryTime().toString() + "Mins");

                imageLoading(shops.getAvatar());

                Picasso.get()
                        .load(shops.getAvatar())
                        .error(R.drawable.ic_leader_board)
                        .placeholder(R.drawable.ic_leader_board)
                        .into(new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                assert restaurantImage != null;
                                restaurantImage.setImageBitmap(bitmap);
                                Palette.from(bitmap).generate(palette -> {
                                    Palette.Swatch textSwatch = palette.getDarkMutedSwatch();
                                    if (textSwatch == null) {
                                        textSwatch = palette.getMutedSwatch();
                                        if (textSwatch != null) {
                                            collapsingToolbar.setContentScrimColor(textSwatch.getRgb());
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                                Window window = getWindow();
                                                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                                                window.setStatusBarColor(textSwatch.getRgb());
                                            }
                                            //headerViewTitle.setTextColor(textSwatch.getTitleTextColor());
                                            //headerViewSubTitle.setTextColor(textSwatch.getBodyTextColor());
                                        }
                                    } else {
                                        collapsingToolbar.setContentScrimColor(textSwatch.getRgb());
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                            Window window = getWindow();
                                            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                                            window.setStatusBarColor(textSwatch.getRgb());
                                        }
                                        // headerViewTitle.setTextColor(textSwatch.getTitleTextColor());
                                        // headerViewSubTitle.setTextColor(textSwatch.getBodyTextColor());
                                    }
                                });

                            }

                            @Override
                            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                                restaurantImage.setImageDrawable(errorDrawable);
                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {
                                restaurantImage.setImageDrawable(placeHolderDrawable);
                            }
                        });

                //Set title
                collapsingToolbar.setTitle(" ");
                toolbarHeaderView.bindTo(shops.getName(), shops.getDescription());
                floatHeaderView.bindTo(shops.getName(), shops.getDescription());
                resturantname_tv.setText(shops.getName());
                resturant_descb_tv.setText(shops.getDescription());

                //Set Categoery shopList adapter
                catagoeryAdapter = new HotelCatagoeryAdapter(this, activity, categoryList);
                accompanimentDishesRv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
                accompanimentDishesRv.setItemAnimator(new DefaultItemAnimator());
                accompanimentDishesRv.setAdapter(catagoeryAdapter);

                if (heartBtn != null)
                    heartBtn.init(this);
                if (shops.getFavorite() != null || isFavourite) {
                    heartBtn.setChecked(true);
                    heartBtn.setTag(1);
                } else
                    heartBtn.setTag(0);
                heartBtn.setShineDistanceMultiple(1.8f);
                heartBtn.setOnClickListener(v -> {
                    if (heartBtn.getTag().equals(0)) {
                        heartBtn.setTag(1);
                        heartBtn.setShapeResource(R.raw.heart);
                    } else {
                        heartBtn.setTag(0);
                        heartBtn.setShapeResource(R.raw.icc_heart);
                    }

                });
                heartBtn.setOnCheckStateChangeListener((view, checked) -> {
                    Log.e("HeartButton", "click " + checked);
                    if (connectionHelper.isConnectingToInternet()) {
                        if (checked) {
                            if (GlobalData.profileModel != null)
                                doFavorite(shops.getId());
                            else {
                                startActivity(new Intent(context, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                overridePendingTransition(R.anim.slide_in_left, R.anim.anim_nothing);
                                finish();
                            }
                        } else {
                            deleteFavorite(shops.getId());
                        }
                    } else {
                        Utils.displayMessage(activity, context, getString(R.string.oops_connect_your_internet));
                    }
                });
            } else {
                startActivity(new Intent(context, SplashActivity.class));
                finish();
            }
        }

        tvSalesSeeAll.setOnClickListener(v -> {
            Intent intent = new Intent(context, SeeAllactivity.class);
            intent.putExtra("CALL_FROM", "SALES");
            intent.putExtra("IS_CATEGORY", isCategory);
            intent.putExtra("IS_CATEGORY_POSITION", categoryPosition);
            context.startActivity(intent);
        });

        tvSeeAllFeatured.setOnClickListener(v -> {
            Intent intent = new Intent(context, SeeAllactivity.class);
            intent.putExtra("CALL_FROM", "FEATURED");
            context.startActivity(intent);
        });

    }

    public void imageLoading(String imgURL) {
        Glide.with(context)
                .load(imgURL)
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.ic_leader_board)
                        .error(R.drawable.ic_leader_board))
                .into(restaurantImage);
    }

    public void getCategoryData() {
        HashMap<String, String> map = new HashMap<>();
        if (GlobalData.profileModel != null) {
            map.put("user_id", String.valueOf(GlobalData.profileModel.getId()));
        }
        Call<CategoryPackages> call = apiInterface.getParticularcCategoryList(categoryPosition, map);
        call.enqueue(new Callback<CategoryPackages>() {
            @Override
            public void onResponse(Call<CategoryPackages> call, Response<CategoryPackages> response) {
//                Log.i("onResponse: ",response.body().toString());
                if (response.isSuccessful()) {
                    if (response.body() != null && response.body().getProducts() != null) {
                        salesProductItems.clear();
                        salesProductItems.addAll(response.body().getProducts());
                        adapterSales.setCategory(true);
                        adapterSales.notifyDataSetChanged();
                    }
                } else
                    Toast.makeText(context, R.string.no_list_in_categpry, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<CategoryPackages> call, Throwable t) {

            }
        });
    }

    private void getRestaurantByShopId() {
        HashMap<String, String> map = new HashMap<>();
        map.put("latitude", String.valueOf(latitude));
        map.put("longitude", String.valueOf(longitude));
        if (GlobalData.profileModel != null) {
            map.put("user_id", String.valueOf(GlobalData.profileModel.getId()));
        }
        try {
            Call<SpecifiedShopResponse> call = apiInterface.getShopById(GlobalData.selectedShop.getId(), map);
            Log.d(TAG, "getRestaurantByShopId: ");
            call.enqueue(new Callback<SpecifiedShopResponse>() {
                @Override
                public void onResponse(Call<SpecifiedShopResponse> call, Response<SpecifiedShopResponse> response) {
                    if (response != null && !response.isSuccessful() && response.errorBody() != null) {
                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            Toast.makeText(context, jObjError.optString("message"), Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    } else if (response.isSuccessful()) {
                        //Check Restaurant list
                        if (response.body().getFeaturedProducts().size() == 0) {
                            llFeaturedProduct.setVisibility(View.GONE);
                        } else {
                            llFeaturedProduct.setVisibility(View.VISIBLE);
                        }

                        featuredProductItems.clear();
                        featuredProductItems.addAll(response.body().getFeaturedProducts());
                        adapterProduct.notifyDataSetChanged();

                        if (response.body().getSaleProducts().size() == 0) {
                            llSalesProduct.setVisibility(View.GONE);
                        } else {
                            llSalesProduct.setVisibility(View.VISIBLE);
                        }

                        salesProductItems.clear();
                        salesProductItems.addAll(response.body().getSaleProducts());
                        adapterSales.notifyDataSetChanged();
                    }
                }

                @Override
                public void onFailure(Call<SpecifiedShopResponse> call, Throwable t) {
                    Toast.makeText(context, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteFavorite(Integer id) {
        Call<Favorite> call = apiInterface.deleteFavorite(id);
        call.enqueue(new Callback<Favorite>() {
            @Override
            public void onResponse(@NonNull Call<Favorite> call, @NonNull Response<Favorite> response) {
                Favorite favorite = response.body();
            }

            @Override
            public void onFailure(@NonNull Call<Favorite> call, @NonNull Throwable t) {

            }
        });

    }

    private void doFavorite(Integer id) {
        Call<Favorite> call = apiInterface.doFavorite(id);
        call.enqueue(new Callback<Favorite>() {
            @Override
            public void onResponse(@NonNull Call<Favorite> call, @NonNull Response<Favorite> response) {
                Favorite favorite = response.body();
            }

            @Override
            public void onFailure(@NonNull Call<Favorite> call, @NonNull Throwable t) {

            }
        });

    }

    public void setViewcartBottomLayout(AddCart addCart) {
        double priceAmount = 0;
        int itemQuantity = 0;
        int itemCount = 0;
        //get Item Count
        itemCount = addCart.getProductList().size();
        for (int i = 0; i < itemCount; i++) {
            //Get Total item Quantity
            itemQuantity = itemQuantity + addCart.getProductList().get(i).getQuantity();
            //Get product price
            if (addCart.getProductList().get(i).getProduct().getPrices() != null)
                priceAmount = priceAmount + (addCart.getProductList().get(i).getQuantity() * addCart.getProductList().get(i).getProduct().getPrices().getOrignalPrice());
            if (addCart.getProductList().get(i).getCartAddons() != null && !addCart.getProductList().get(i).getCartAddons().isEmpty()) {
                for (int j = 0; j < addCart.getProductList().get(i).getCartAddons().size(); j++) {
                    if (addCart.getProductList().get(i).getCartAddons().get(j).getAddonProduct() != null) {
                        priceAmount = priceAmount + (addCart.getProductList().get(i).getQuantity()
                                * (addCart.getProductList().get(i).getCartAddons().get(j).getQuantity()
                                * addCart.getProductList().get(i).getCartAddons().get(j).getAddonProduct().getPrice()));
                    }
                }
            }
        }
        GlobalData.notificationCount = itemQuantity;
        if (itemQuantity == 0) {
            viewCartLayout.setVisibility(View.GONE);
            // Start animation
            viewCartLayout.startAnimation(slide_down);
        } else if (itemQuantity == 1) {
            itemText.setText(itemQuantity + getString(R.string.items__) + numberFormat.format(priceAmount));
            if (!isCategory) {
                if (!shops.getId().equals(GlobalData.addCart.getProductList().get(0).getProduct().getShopId())) {
                    viewCartShopName.setText(getString(R.string.from) + GlobalData.addCart.getProductList().get(0).getProduct().getShop().getName());
                } else
                    viewCartShopName.setVisibility(View.GONE);
            } else {
                viewCartShopName.setText(getString(R.string.from) + GlobalData.addCart.getProductList().get(0).getProduct().getShop().getName());
            }
            if (viewCartLayout.getVisibility() == View.GONE) {
                // Start animation
                viewCartLayout.setVisibility(View.VISIBLE);
                viewCartLayout.startAnimation(slide_up);
            }
        } else {
            itemText.setText(itemQuantity + getString(R.string.items__) + numberFormat.format(priceAmount));
            viewCartShopName.setVisibility(View.VISIBLE);
            if (!isCategory) {
                if (!shops.getId().equals(GlobalData.addCart.getProductList().get(0).getProduct().getShopId())) {
                    viewCartShopName.setText(getString(R.string.from) + GlobalData.addCart.getProductList().get(0).getProduct().getShop().getName());
                } else
                    viewCartShopName.setVisibility(View.GONE);
            } else {
                viewCartShopName.setText(getString(R.string.from) + GlobalData.addCart.getProductList().get(0).getProduct().getShop().getName());
            }
            if (viewCartLayout.getVisibility() == View.GONE) {
                // Start animation
                viewCartLayout.setVisibility(View.VISIBLE);
                viewCartLayout.startAnimation(slide_up);
            }
        }
    }

    private void getCategories(HashMap<String, String> map) {
        skeleton.show();
        Call<User> getprofile = apiInterface.getProfile(map);
        getprofile.enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
                skeleton.hide();
                if (response.isSuccessful()) {
                    SharedHelper.putKey(context, "logged", "true");
                    GlobalData.profileModel = response.body();
                    //addCart = new AddCart();
                    if (response.body().getCart().size() > 0) {
                        addCart = new AddCart();
                        addCart.setProductList(response.body().getCart());
                    }
                    GlobalData.addressList = new AddressList();
                    GlobalData.addressList.setAddresses(response.body().getAddresses());
                    if (addCart != null && addCart.getProductList().size() != 0)
                        GlobalData.addCartShopId = addCart.getProductList().get(0).getProduct().getShopId();
                    if (GlobalData.addCart != null && GlobalData.addCart.getProductList().size() != 0) {
                        setViewcartBottomLayout(GlobalData.addCart);
                    } else {
                        viewCartLayout.setVisibility(View.GONE);
                    }
                } else {
                    if (response.code() == 401) {
                        SharedHelper.putKey(context, "logged", "false");
                        startActivity(new Intent(context, LoginActivity.class));
                        finish();
                    }
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().toString());
                        Toast.makeText(context, jObjError.optString("error"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {
                skeleton.hide();
            }
        });
    }

    /*@Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }*/

    @Override
    protected void onResume() {
        super.onResume();
        if (bottomSheetDialogFragment != null)
            bottomSheetDialogFragment.dismiss();

        if (GlobalData.profileModel != null) {
            getCategories(new HashMap<>());
        }

        if (connectionHelper.isConnectingToInternet()) {
            if (!isCategory) {
                getRestaurantByShopId();
                if (GlobalData.addCart != null && GlobalData.addCart.getProductList().size() != 0) {
                    viewCartLayout.setVisibility(View.VISIBLE);
                } else {
                    viewCartLayout.setVisibility(View.GONE);
                }
            } else {
                getCategoryData();
            }
        } else {
            Utils.displayMessage(activity, context, getString(R.string.oops_connect_your_internet));
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.anim_nothing, R.anim.slide_out_right);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(verticalOffset) / (float) maxScroll;
        if (percentage == 1f && isHideToolbarView) {
            toolbarHeaderView.setVisibility(View.VISIBLE);
            isHideToolbarView = !isHideToolbarView;
        } else if (percentage < 1f && !isHideToolbarView) {
            toolbarHeaderView.setVisibility(View.GONE);
            isHideToolbarView = !isHideToolbarView;
        }
    }
}

