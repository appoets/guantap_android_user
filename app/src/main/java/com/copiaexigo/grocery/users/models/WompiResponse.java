package com.copiaexigo.grocery.users.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WompiResponse {
    @SerializedName("payment_url")
    @Expose
    private String paymentUrl;

    public String getPaymentUrl() {
        return paymentUrl;
    }

    public void setPaymentUrl(String paymentUrl) {
        this.paymentUrl = paymentUrl;
    }
}
