package com.copiaexigo.grocery.users.fragments;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.copiaexigo.grocery.users.R;
import com.copiaexigo.grocery.users.adapter.OrderDetailAdapter;
import com.copiaexigo.grocery.users.helper.GlobalData;
import com.copiaexigo.grocery.users.models.Item;
import com.copiaexigo.grocery.users.models.Order;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderDetailFragment extends Fragment {


    @BindView(R.id.order_recycler_view)
    RecyclerView orderRecyclerView;
    Unbinder unbinder;

    @BindView(R.id.item_total_amount)
    TextView itemTotalAmount;
    @BindView(R.id.service_tax)
    TextView serviceTax;
    @BindView(R.id.delivery_charges)
    TextView deliveryCharges;
    @BindView(R.id.total_amount)
    TextView totalAmount;
    @BindView(R.id.discount_amount)
    TextView discountAmount;
    @BindView(R.id.wallet_amount_detection)
    TextView walletAmountDetection;

    private final Context context = getActivity();
    private final NumberFormat numberFormat = GlobalData.getNumberFormat();

    public OrderDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_order_detail, container, false);
        unbinder = ButterKnife.bind(this, view);
        Order order = GlobalData.isSelectedOrder;
        //set Item List Values
        List<Item> itemList = new ArrayList<>();
        if (order != null) {
            itemList.addAll(order.getItems());
            //Offer Restaurant Adapter
            orderRecyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
            orderRecyclerView.setItemAnimator(new DefaultItemAnimator());
            orderRecyclerView.setHasFixedSize(true);
            OrderDetailAdapter orderItemListAdapter = new OrderDetailAdapter(itemList, context, numberFormat);
            orderRecyclerView.setAdapter(orderItemListAdapter);
            itemTotalAmount.setText(numberFormat.format(order.getInvoice().getGross()));
            serviceTax.setText(numberFormat.format(order.getInvoice().getTax()));
            deliveryCharges.setText(numberFormat.format(order.getInvoice().getDeliveryCharge()));
            discountAmount.setText(numberFormat.format(order.getInvoice().getDiscount()));
            walletAmountDetection.setText(numberFormat.format(order.getInvoice().getWalletAmount()));
            totalAmount.setText(numberFormat.format(order.getInvoice().getPayable()));
        }
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
