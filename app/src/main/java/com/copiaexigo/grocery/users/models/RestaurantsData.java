
package com.copiaexigo.grocery.users.models;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class RestaurantsData {

    @SerializedName("shops")
    private List<Shop> shops =new ArrayList<>();

    @SerializedName("currency")
    private String currency;

    @SerializedName("banners")
    private List<Banner> banners=new ArrayList<>();

    public void setShops(List<Shop> shops){
        this.shops = shops;
    }

    public List<Shop> getShops(){
        return shops;
    }

    public void setCurrency(String currency){
        this.currency = currency;
    }

    public String getCurrency(){
        return currency;
    }

    public void setBanners(List<Banner> banners){
        this.banners = banners;
    }

    public List<Banner> getBanners(){
        return banners;
    }

}
