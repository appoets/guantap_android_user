
package com.copiaexigo.grocery.users.models;

import com.copiaexigo.grocery.users.models.addonsdata.AddonGroups;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Product {

    @SerializedName("out_of_stock")
    private String outOfStock;

    @SerializedName("featured")
    private Integer featured;

    @SerializedName("images")
    private List<Image> images;

    @SerializedName("cart")
    @Expose
    private List<Cart> cart;

    @SerializedName("max_quantity")
    private Integer maxQuantity;

    @SerializedName("addons")
    private List<Addon> addons;

    public List<Cart> getCart() {
        return cart;
    }

    @SerializedName("shop")
    @Expose
    private Shop shop;

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public void setCart(List<Cart> cart) {
        this.cart = cart;
    }

    @SerializedName("featured_position")
    private Integer featuredPosition;

    @SerializedName("description")
    private String description;

    @SerializedName("cuisine_id")
    private Object cuisineId;

    @SerializedName("avalability")
    private Integer avalability;

    @SerializedName("food_type")
    private String foodType;

    @SerializedName("shop_id")
    private Integer shopId;

    @SerializedName("addon_status")
    private Integer addonStatus;

    @SerializedName("name")
    private String name;

    @SerializedName("id")
    private Integer id;

    @SerializedName("position")
    private Integer position;

    @SerializedName("prices")
    private Prices prices;

    @SerializedName("status")
    private String status;

    @SerializedName("addongroup")
    private List<AddonGroups> addonGroups;

    public void setOutOfStock(String outOfStock) {
        this.outOfStock = outOfStock;
    }

    public String getOutOfStock() {
        return outOfStock;
    }

    public void setFeatured(Integer featured) {
        this.featured = featured;
    }

    public Integer getFeatured() {
        return featured;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setMaxQuantity(Integer maxQuantity) {
        this.maxQuantity = maxQuantity;
    }

    public Integer getMaxQuantity() {
        return maxQuantity;
    }

    public void setAddons(List<Addon> addons) {
        this.addons = addons;
    }

    public List<Addon> getAddons() {
        return addons;
    }

    public void setFeaturedPosition(Integer featuredPosition) {
        this.featuredPosition = featuredPosition;
    }

    public Integer getFeaturedPosition() {
        return featuredPosition;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setCuisineId(Object cuisineId) {
        this.cuisineId = cuisineId;
    }

    public Object getCuisineId() {
        return cuisineId;
    }

    public void setAvalability(Integer avalability) {
        this.avalability = avalability;
    }

    public Integer getAvalability() {
        return avalability;
    }

    public void setFoodType(String foodType) {
        this.foodType = foodType;
    }

    public String getFoodType() {
        return foodType;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setAddonStatus(Integer addonStatus) {
        this.addonStatus = addonStatus;
    }

    public Integer getAddonStatus() {
        return addonStatus;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPrices(Prices prices) {
        this.prices = prices;
    }

    public Prices getPrices() {
        return prices;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public List<AddonGroups> getAddonGroups() {
        return addonGroups;
    }

    public void setAddonGroups(List<AddonGroups> addonGroups) {
        this.addonGroups = addonGroups;
    }
}