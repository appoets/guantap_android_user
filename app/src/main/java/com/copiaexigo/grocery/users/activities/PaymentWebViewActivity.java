package com.copiaexigo.grocery.users.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.copiaexigo.grocery.users.R;
import com.copiaexigo.grocery.users.build.api.ApiClient;
import com.copiaexigo.grocery.users.build.api.ApiInterface;
import com.copiaexigo.grocery.users.helper.CustomDialog;
import com.copiaexigo.grocery.users.models.Order;

import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PaymentWebViewActivity extends AppCompatActivity {

    @BindView(R.id.web_view)
    WebView paymentWebView;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.title)
    TextView title;

    private static final String TAG = PaymentWebViewActivity.class.getSimpleName();
    private CustomDialog customDialog;
    private boolean isOrderWallet = false;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_webview);
        ButterKnife.bind(this);
        customDialog = new CustomDialog(this);
        customDialog.show();
        title.setText("Wompi Pay");
        String paymentUrl = getIntent().getStringExtra("paymentUrl");
        isOrderWallet = getIntent().getBooleanExtra("is_order_check", false);
        paymentWebView.getSettings().setJavaScriptEnabled(true);
        paymentWebView.getSettings().setLoadWithOverviewMode(true);
        paymentWebView.getSettings().setUseWideViewPort(true);
        paymentWebView.setWebChromeClient(new WebChromeClient());
        paymentWebView.setWebViewClient(new MyWebViewClient());

        paymentWebView.loadUrl(paymentUrl);
        // back.setOnClickListener(v -> onBackPressed());
    }

    private class MyWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            if (customDialog != null && !customDialog.isShowing()) {
                customDialog.show();
            }
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            Log.d(TAG, "onPageFinished   =>" + url);

            if (view.getUrl().contains("https://shop.guantap.com/payment/callback") || view.getUrl().contains("https://shop.guantap.com")) {
                paymentWebView.setVisibility(View.GONE);
                if (!isOrderWallet) {
                    setResult(RESULT_OK);
                    finish();
                    return;
                }
                getOngoingOrders();
                CookieManager.getInstance().removeAllCookies(null);
            }
            if (customDialog != null && customDialog.isShowing())
                customDialog.dismiss();

        }


        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            return super.shouldOverrideUrlLoading(view, request);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.d(TAG, "shouldOverrideUrlLoading: " + url.toString());
            if (url.contains("intent://")) {
                paymentWebView.setVisibility(View.GONE);
                try {
                    Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                    String fallbackUrl = intent.getStringExtra("browser_fallback_url");
                    if (fallbackUrl != null) {
                        view.loadUrl(fallbackUrl);
                    }
                } catch (URISyntaxException e) {
                    //not an intent uri
                }
            } else {
                paymentWebView.setVisibility(View.VISIBLE);
            }
            return super.shouldOverrideUrlLoading(view, url);

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBackPressed() {
        alertCancel();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void alertCancel() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.are_you_sure_cancel_payment)
                .setPositiveButton(getResources().getString(R.string.yes), (dialog, which) -> {
                    // continue with delete
                    setResult(RESULT_CANCELED);
                    finish();
                    CookieManager.getInstance().removeAllCookies(null);
                }).setNegativeButton(getResources().getString(R.string.cancel), (dialog, which) -> {
            // do nothing
            dialog.dismiss();
        });
        AlertDialog alert = builder.create();
        alert.show();
        Button nButton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        nButton.setTextColor(ContextCompat.getColor(this, R.color.theme));
        nButton.setTypeface(nButton.getTypeface(), Typeface.BOLD);
        Button dButton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        dButton.setTextColor(ContextCompat.getColor(this, R.color.theme));
        dButton.setTypeface(dButton.getTypeface(), Typeface.BOLD);
    }

    private void getOngoingOrders() {
        if (customDialog != null && !customDialog.isShowing()) {
            customDialog.show();
        }
        Call<List<Order>> call = ApiClient.getRetrofit().create(ApiInterface.class).getOngoingOrders();
        call.enqueue(new Callback<List<Order>>() {
            @Override
            public void onResponse(@NonNull Call<List<Order>> call, @NonNull Response<List<Order>> response) {
                if (customDialog != null && customDialog.isShowing())
                    customDialog.dismiss();
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    if (!response.body().isEmpty()) {
                        startActivity(new Intent(PaymentWebViewActivity.this, OrdersActivity.class)
                                .putExtra("isProfile", false)
                                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                        finish();
                    } else {
                        Toast.makeText(PaymentWebViewActivity.this, R.string.payment_failed, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(PaymentWebViewActivity.this, jObjError.optString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(PaymentWebViewActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Order>> call, @NonNull Throwable t) {
                Toast.makeText(PaymentWebViewActivity.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                if (customDialog != null && customDialog.isShowing())
                    customDialog.dismiss();
            }
        });

    }

}