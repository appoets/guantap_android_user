package com.copiaexigo.grocery.users.models.specifiedshop;

import com.google.gson.annotations.SerializedName;

public class Prices{

	@SerializedName("price")
	private Double price;

	@SerializedName("discount")
	private Double discount;

	@SerializedName("currency")
	private String currency;

	@SerializedName("id")
	private Integer id;

	@SerializedName("discount_type")
	private String discountType;

	@SerializedName("orignal_price")
	private Double orignalPrice;

	public void setPrice(Double price){
		this.price = price;
	}

	public Double getPrice(){
		return price;
	}

	public void setDiscount(Double discount){
		this.discount = discount;
	}

	public Double getDiscount(){
		return discount;
	}

	public void setCurrency(String currency){
		this.currency = currency;
	}

	public String getCurrency(){
		return currency;
	}

	public void setId(Integer id){
		this.id = id;
	}

	public Integer getId(){
		return id;
	}

	public void setDiscountType(String discountType){
		this.discountType = discountType;
	}

	public String getDiscountType(){
		return discountType;
	}

	public void setOrignalPrice(Double orignalPrice){
		this.orignalPrice = orignalPrice;
	}

	public Double getOrignalPrice(){
		return orignalPrice;
	}
}