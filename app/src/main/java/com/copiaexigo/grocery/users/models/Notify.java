package com.copiaexigo.grocery.users.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Notify {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("send_to")
    @Expose
    private String sendTo;
    @SerializedName("condition")
    @Expose
    private String condition;
    @SerializedName("condition_data")
    @Expose
    private String conditionData;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("sent_to")
    @Expose
    private Integer sentTo;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("schedule_at")
    @Expose
    private Object scheduleAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSendTo() {
        return sendTo;
    }

    public void setSendTo(String sendTo) {
        this.sendTo = sendTo;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getConditionData() {
        return conditionData;
    }

    public void setConditionData(String conditionData) {
        this.conditionData = conditionData;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getSentTo() {
        return sentTo;
    }

    public void setSentTo(Integer sentTo) {
        this.sentTo = sentTo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getScheduleAt() {
        return scheduleAt;
    }

    public void setScheduleAt(Object scheduleAt) {
        this.scheduleAt = scheduleAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }


}
