package com.copiaexigo.grocery.users.adapter;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat;

import com.copiaexigo.grocery.users.R;
import com.copiaexigo.grocery.users.build.api.ApiClient;
import com.copiaexigo.grocery.users.build.api.ApiInterface;
import com.copiaexigo.grocery.users.fragments.AddonBottomSheetFragment;
import com.copiaexigo.grocery.users.fragments.CartChoiceModeFragment;
import com.copiaexigo.grocery.users.fragments.CartFragment;
import com.copiaexigo.grocery.users.helper.GlobalData;
import com.copiaexigo.grocery.users.models.AddCart;
import com.copiaexigo.grocery.users.models.Cart;
import com.copiaexigo.grocery.users.models.CartAddon;
import com.copiaexigo.grocery.users.models.Product;
import com.copiaexigo.grocery.users.models.Shop;
import com.robinhood.ticker.TickerUtils;
import com.robinhood.ticker.TickerView;

import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.copiaexigo.grocery.users.fragments.CartChoiceModeFragment.lastCart;

/*
 * Created by santhosh@appoets.com on 22-08-2017.
 */

public class ViewCartAdapter extends RecyclerView.Adapter<ViewCartAdapter.MyViewHolder> {
    private final List<Cart> list;
    public static Context context;
    private double priceAmount = 0;
    public static int discount = 0;
    public static int itemCount = 0;
    public static int itemQuantity = 0;
    public static Product product;
    public static boolean dataResponse = false;
    public static Cart productList;
    public static ApiInterface apiInterface = ApiClient.getRetrofit().create(ApiInterface.class);
    public static AddCart addCart;
    public static AnimatedVectorDrawableCompat avdProgress;
    public static Dialog dialog;
    public static Runnable action;
    public static Shop selectedShop = GlobalData.selectedShop;
    public static CartChoiceModeFragment bottomSheetDialogFragment;

    NumberFormat numberFormat = GlobalData.getNumberFormat();
    //Animation number
    private static final char[] NUMBER_LIST = TickerUtils.getDefaultNumberList();

    public ViewCartAdapter(List<Cart> list, Context con) {
        this.list = list;
        context = con;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_product_item, parent, false);
        return new MyViewHolder(itemView);
    }

    public void add(Cart item, int position) {
        list.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(Cart item) {
        int position = list.indexOf(item);
        list.remove(position);
        notifyItemRemoved(position);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.cardAddTextLayout.setVisibility(View.GONE);
        holder.cardAddDetailLayout.setVisibility(View.VISIBLE);
        product = list.get(position).getProduct();
        holder.cardTextValueTicker.setCharacterList(NUMBER_LIST);
        holder.dishNameTxt.setText(product.getName());
        holder.cardTextValue.setText(list.get(position).getQuantity().toString());
        holder.cardTextValueTicker.setText(list.get(position).getQuantity().toString());
        priceAmount = list.get(position).getQuantity() * product.getPrices().getOrignalPrice();
        holder.priceTxt.setText(numberFormat.format(priceAmount));

        if (!product.getFoodType().equalsIgnoreCase("veg")) {
            holder.foodImageType.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_nonveg));
        } else {
            holder.foodImageType.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_veg));
        }
        selectedShop = product.getShop();

        List<CartAddon> cartAddonList = list.get(position).getCartAddons();
        if (cartAddonList.isEmpty()) {
            holder.layoutAddons.setVisibility(View.VISIBLE);
        } else {
            holder.layoutAddons.removeAllViews();
            for (int i = 0; i < cartAddonList.size(); i++) {
                if (cartAddonList.get(i).getAddonProduct() != null) {
                    View itemView = LayoutInflater.from(holder.itemView.getContext()).inflate(R.layout.list_addon, null, false);
                    TextView addonName = itemView.findViewById(R.id.addon_name);
                    TextView addonPrice = itemView.findViewById(R.id.addon_price);
                    CartAddon object = cartAddonList.get(i);
                    if (object.getAddonProduct() != null) {
                        String value = context.getString(R.string.addon_,
                                object.getAddonProduct().getAddon().getName(),
                                object.getQuantity(),
                                numberFormat.format(object.getAddonProduct().getPrice()));
                        addonName.setText(value);
                        Double totalAmount = Double.valueOf(object.getAddonProduct().getPrice() * object.getQuantity());
                        addonPrice.setText(numberFormat.format(totalAmount));
                    }
                    holder.layoutAddons.addView(itemView);
                }
            }
        }


        holder.cardAddBtn.setOnClickListener(v -> {
            Log.e("access_token2", GlobalData.accessToken);
            /* Intilaize Animation View Image */
            holder.animationLineCartAdd.setVisibility(View.VISIBLE);
            //Intialize
            avdProgress = AnimatedVectorDrawableCompat.create(context, R.drawable.add_cart_avd_line);
            holder.animationLineCartAdd.setBackground(avdProgress);
            avdProgress.start();
            action = () -> {
                if (!dataResponse) {
                    avdProgress.start();
                    holder.animationLineCartAdd.postDelayed(action, 3000);
                }

            };
            holder.animationLineCartAdd.postDelayed(action, 3000);
            /* Press Add Card Add button */
            product = list.get(position).getProduct();
            if (product.getAddons() != null && !product.getAddons().isEmpty()) {
                GlobalData.isSelectedProduct = product;
                lastCart = list.get(position);
                if (lastCart.getCartAddons() != null && !lastCart.getCartAddons().isEmpty()) {
                    CartChoiceModeFragment.lastCart = list.get(position);
                    bottomSheetDialogFragment = new CartChoiceModeFragment();
                    bottomSheetDialogFragment.show(((AppCompatActivity) context).getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
                    CartChoiceModeFragment.isViewcart = true;
                    CartChoiceModeFragment.isSearch = false;
                } else {
                    int countValue = Integer.parseInt(holder.cardTextValue.getText().toString()) + 1;
                    holder.cardTextValue.setText("" + countValue);
                    holder.cardTextValueTicker.setText("" + countValue);
                    HashMap<String, String> map = new HashMap<>();
                    map.put("product_id", product.getId().toString());
                    map.put("quantity", holder.cardTextValue.getText().toString());
                    map.put("cart_id", String.valueOf(list.get(position).getId()));
                    Log.e("AddCart_add", map.toString());
                    addCart(map);
                    int quantity = Integer.parseInt(holder.cardTextValue.getText().toString());
                    priceAmount = quantity * product.getPrices().getOrignalPrice();
                    holder.priceTxt.setText(numberFormat.format(priceAmount));
                }
            } else {
                int countValue = Integer.parseInt(holder.cardTextValue.getText().toString()) + 1;
                holder.cardTextValue.setText("" + countValue);
                holder.cardTextValueTicker.setText("" + countValue);
                HashMap<String, String> map = new HashMap<>();
                map.put("product_id", product.getId().toString());
                map.put("quantity", holder.cardTextValue.getText().toString());
                map.put("cart_id", String.valueOf(list.get(position).getId()));
                Log.e("AddCart_add", map.toString());
                addCart(map);
                int quantity = Integer.parseInt(holder.cardTextValue.getText().toString());
                priceAmount = quantity * product.getPrices().getOrignalPrice();
                holder.priceTxt.setText(numberFormat.format(priceAmount));
            }
        });

        holder.cardMinusBtn.setOnClickListener(v -> {
            /* Intilaize Animation View Image */
            holder.animationLineCartAdd.setVisibility(View.VISIBLE);
            //Intialize
            avdProgress = AnimatedVectorDrawableCompat.create(context, R.drawable.add_cart_avd_line);
            holder.animationLineCartAdd.setBackground(avdProgress);
            avdProgress.start();
            action = () -> {
                if (!dataResponse) {
                    avdProgress.start();
                    holder.animationLineCartAdd.postDelayed(action, 3000);
                }
            };
            holder.animationLineCartAdd.postDelayed(action, 3000);
            int countMinusValue;
            /* Press Add Card Minus button */
            product = list.get(position).getProduct();
            int quantity = Integer.parseInt(holder.cardTextValue.getText().toString());
            priceAmount = quantity * product.getPrices().getOrignalPrice();
            holder.priceTxt.setText(numberFormat.format(priceAmount));

            if (holder.cardTextValue.getText().toString().equalsIgnoreCase("1")) {
                countMinusValue = Integer.parseInt(holder.cardTextValue.getText().toString()) - 1;
                holder.cardTextValue.setText("" + countMinusValue);
                holder.cardTextValueTicker.setText("" + countMinusValue);
                productList = list.get(position);
                HashMap<String, String> map = new HashMap<>();
                map.put("product_id", product.getId().toString());
                map.put("quantity", String.valueOf(countMinusValue));
                map.put("cart_id", String.valueOf(list.get(position).getId()));
                List<CartAddon> cartAddonList1 = list.get(position).getCartAddons();
                for (int i = 0; i < cartAddonList1.size(); i++) {
                    CartAddon cartAddon = cartAddonList1.get(i);
                    map.put("product_addons[" + cartAddon.getAddonProduct().getId() + "]", cartAddon.getAddonProduct().getId().toString());
                    map.put("addons_qty[" + cartAddon.getAddonProduct().getId() + "]", (cartAddon.getQuantity() - 1) + "");
                }
                Log.e("AddCart_Minus", map.toString());
                addCart(map);
                remove(productList);
            } else {
                countMinusValue = Integer.parseInt(holder.cardTextValue.getText().toString()) - 1;
                holder.cardTextValue.setText("" + countMinusValue);
                holder.cardTextValueTicker.setText("" + countMinusValue);
                HashMap<String, String> map = new HashMap<>();
                map.put("product_id", product.getId().toString());
                map.put("quantity", String.valueOf(countMinusValue));
                map.put("cart_id", String.valueOf(list.get(position).getId()));
                List<CartAddon> cartAddonList1 = list.get(position).getCartAddons();
                for (int i = 0; i < cartAddonList1.size(); i++) {
                    CartAddon cartAddon = cartAddonList1.get(i);
                    if (cartAddon.getAddonProduct() != null) {
                        map.put("product_addons[" + cartAddon.getAddonProduct().getId() + "]", cartAddon.getAddonProduct().getId().toString());
                        map.put("addons_qty[" + cartAddon.getAddonProduct().getId() + "]", map.get("quantity"));
                        map.put("addons_price[" + cartAddon.getAddonProduct().getId() + "]", "" + cartAddon.getAddonProduct().getPrice());
                    }
                }
                Log.e("AddCart_Minus", map.toString());
                addCart(map);
            }
        });

        holder.customize.setOnClickListener(v -> {
            /* Intilaize Animation View Image */
            holder.animationLineCartAdd.setVisibility(View.VISIBLE);
            //Intialize
            avdProgress = AnimatedVectorDrawableCompat.create(context, R.drawable.add_cart_avd_line);
            holder.animationLineCartAdd.setBackground(avdProgress);
            avdProgress.start();
            action = new Runnable() {
                @Override
                public void run() {
                    if (!dataResponse) {
                        avdProgress.start();
                        holder.animationLineCartAdd.postDelayed(action, 3000);
                    }

                }
            };
            holder.animationLineCartAdd.postDelayed(action, 3000);
            productList = list.get(position);
            GlobalData.isSelectedProduct = product;
            GlobalData.isSelctedCart = productList;
            GlobalData.cartAddons = productList.getCartAddons();
            AddonBottomSheetFragment bottomSheetDialogFragment = new AddonBottomSheetFragment();
            bottomSheetDialogFragment.show(((AppCompatActivity) context).getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
            AddonBottomSheetFragment.selectedCart = list.get(position);
            // Right here!
        });

    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView dishImg;
        private final ImageView foodImageType;
        private final ImageView cardAddBtn;
        private final ImageView cardMinusBtn;
        private final ImageView animationLineCartAdd;
        private final TextView dishNameTxt;
        private final TextView priceTxt;
        private final TextView cardTextValue;
        private final TextView cardAddInfoText;
        private final TextView cardAddOutOfStock;
        private final TextView customizableTxt;
        private final TextView addons;
        private final TextView customize;
        private final TickerView cardTextValueTicker;
        private final RelativeLayout cardAddDetailLayout;
        private final RelativeLayout cardAddTextLayout;
        private final LinearLayout layoutAddons;
        private RelativeLayout cardInfoLayout;

        private MyViewHolder(View view) {
            super(view);
            foodImageType = itemView.findViewById(R.id.food_type_image);
            animationLineCartAdd = itemView.findViewById(R.id.animation_line_cart_add);
            dishNameTxt = itemView.findViewById(R.id.dish_name_text);
            priceTxt = itemView.findViewById(R.id.price_text);
            customizableTxt = itemView.findViewById(R.id.customizable_txt);
            addons = itemView.findViewById(R.id.addons);
            customize = itemView.findViewById(R.id.customize);
            /*    Add card Button Layout*/
            cardAddDetailLayout = itemView.findViewById(R.id.add_card_layout);
            cardAddTextLayout = itemView.findViewById(R.id.add_card_text_layout);
            cardAddInfoText = itemView.findViewById(R.id.avialablity_time);
            cardAddOutOfStock = itemView.findViewById(R.id.out_of_stock);
            cardAddBtn = itemView.findViewById(R.id.card_add_btn);
            cardMinusBtn = itemView.findViewById(R.id.card_minus_btn);
            cardTextValue = itemView.findViewById(R.id.card_value);
            layoutAddons = itemView.findViewById(R.id.layoutAddons);
            cardTextValueTicker = itemView.findViewById(R.id.card_value_ticker);
        }
    }


    public static void addCart(HashMap<String, String> map) {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.empty_dialog);
        dialog.setCancelable(false);
        dataResponse = false;
        dialog.show();
        NumberFormat numberFormat = GlobalData.getNumberFormat();
        Call<AddCart> call = apiInterface.postAddCart(map);
        call.enqueue(new Callback<AddCart>() {
            @Override
            public void onResponse(Call<AddCart> call, Response<AddCart> response) {
                avdProgress.stop();
                dialog.dismiss();
                dataResponse = true;
                if (response != null && !response.isSuccessful() && response.errorBody() != null) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(context, jObjError.optString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else if (response.isSuccessful()) {
                    addCart = response.body();
                    GlobalData.addCart = new AddCart();
                    GlobalData.addCart = response.body();
                    CartFragment.viewCartItemList.clear();
                    CartFragment.viewCartItemList.addAll(response.body().getProductList());
                    CartFragment.viewCartAdapter.notifyDataSetChanged();

                    itemQuantity = 0;
                    itemCount = 0;
                    //get Item Count
                    itemCount = addCart.getProductList().size();
                    if (itemCount != 0) {
                        for (int i = 0; i < itemCount; i++) {
                            //Get Total item Quantity
                            itemQuantity = itemQuantity + addCart.getProductList().get(i).getQuantity();
                        }
                        GlobalData.notificationCount = itemQuantity;
                        //Set Payment details
                        CartFragment.itemTotalAmount.setText(numberFormat.format(addCart.getTotalPrice()));
                        CartFragment.deliveryCharges.setText(numberFormat.format(addCart.getDeliveryCharges()));
                        CartFragment.discountAmount.setText(numberFormat.format(addCart.getShopDiscount()));
                        CartFragment.serviceTax.setText(numberFormat.format(addCart.getTax()));
                        CartFragment.payAmount.setText(numberFormat.format(addCart.getPayable()));
                    } else {
                        GlobalData.notificationCount = itemQuantity;
                        CartFragment.errorLayout.setVisibility(View.VISIBLE);
                        CartFragment.dataLayout.setVisibility(View.GONE);
                        Toast.makeText(context, "Cart is empty", Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void onFailure(Call<AddCart> call, Throwable t) {

            }
        });
    }
}
