package com.copiaexigo.grocery.users.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.copiaexigo.grocery.users.BuildConfig;
import com.copiaexigo.grocery.users.HomeActivity;
import com.copiaexigo.grocery.users.R;
import com.copiaexigo.grocery.users.activities.AccountPaymentActivity;
import com.copiaexigo.grocery.users.activities.EditAccountActivity;
import com.copiaexigo.grocery.users.activities.FavouritesActivity;
import com.copiaexigo.grocery.users.activities.LoginActivity;
import com.copiaexigo.grocery.users.activities.ManageAddressActivity;
import com.copiaexigo.grocery.users.activities.NotificationActivity;
import com.copiaexigo.grocery.users.activities.OrdersActivity;
import com.copiaexigo.grocery.users.activities.ReferFriendActivity;
import com.copiaexigo.grocery.users.activities.WelcomeScreenActivity;
import com.copiaexigo.grocery.users.adapter.ProfileSettingsAdapter;
import com.copiaexigo.grocery.users.helper.GlobalData;
import com.copiaexigo.grocery.users.helper.SharedHelper;
import com.copiaexigo.grocery.users.utils.ListViewSizeHelper;
import com.copiaexigo.grocery.users.utils.LocaleUtils;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by santhosh@appoets.com on 22-08-2017.
 */
@SuppressLint("NonConstantResourceId")
public class ProfileFragment extends Fragment {
    /* @BindView(R.id.text_line)
     TextView textLine;*/
    @BindView(R.id.app_version)
    TextView appVersion;
    /*    @BindView(R.id.view_line)
        View viewLine;*/
  /*  @BindView(R.id.logout)
    Button logout;*/
    @BindView(R.id.arrow_image)
    ImageView arrowImage;
    @BindView(R.id.list_layout)
    RelativeLayout listLayout;
    @BindView(R.id.myaccount_layout)
    LinearLayout myAccountLayout;
    @BindView(R.id.error_layout)
    RelativeLayout errorLayout;
    @BindView(R.id.login_btn)
    Button loginBtn;
    @BindView(R.id.profile_setting_lv)
    ListView profileSettingLv;

    private Activity activity;
    private Context context;
    private ViewGroup toolbar;
    private View toolbarLayout;
    private ImageView userImage;
    private TextView userName;
    private TextView userPhone;
    private TextView userEmail;
    private static final int REQUEST_LOCATION = 1450;
    private GoogleApiClient mGoogleApiClient;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.activity = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        userImage = (ImageView) view.findViewById(R.id.user_image);
        userName = (TextView) view.findViewById(R.id.user_name);
        //userPhone = (TextView) view.findViewById(R.id.user_phone);
        userEmail = (TextView) view.findViewById(R.id.user_mail);
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        HomeActivity.updateNotificationCount(context, GlobalData.notificationCount);
        initView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (toolbar != null) {
            toolbar.removeView(toolbarLayout);
        }
    }

    private void openSettingPage(int position) {
        switch (position) {
            case 0:
                startActivity(new Intent(context, EditAccountActivity.class));
                break;
            case 1:
                startActivity(new Intent(context, ManageAddressActivity.class));
                break;
            case 2:
                startActivity(new Intent(context, FavouritesActivity.class));
                break;
            case 3:
                startActivity(new Intent(context, OrdersActivity.class));
                break;
            case 4:
                startActivity(new Intent(context, AccountPaymentActivity.class)
                        .putExtra("is_show_wallet", true)
                        .putExtra("is_show_cash", false));
                break;
            case 5:
                startActivity(new Intent(context, NotificationActivity.class));
                break;
            case 6:
                changeLanguage();
                break;
            case 7:
                startActivity(new Intent(context, ReferFriendActivity.class));
                break;
               /* startActivity(new Intent(context, PromotionActivity.class));
                break;*/
            case 8:
                alertDialog();
                //startActivity(new Intent(context, ChangePasswordActivity.class));
//
//            case 5:
//                startActivity(new Intent(context, PromotionActivity.class));
//                break;

         /*   case 8:
                alertDialog();*/
            default:
        }
        getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.anim_nothing);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        System.out.println("ProfileFragment");
        toolbar = getActivity().findViewById(R.id.toolbar);
        if (GlobalData.profileModel != null) {
            toolbarLayout = LayoutInflater.from(context).inflate(R.layout.toolbar_profile, toolbar, false);
            initView();
          /*  userImage = (ImageView) toolbarLayout.findViewById(R.id.user_image);
            userName = (TextView) toolbarLayout.findViewById(R.id.user_name);
            userPhone = (TextView) toolbarLayout.findViewById(R.id.user_phone);
            userEmail = (TextView) toolbarLayout.findViewById(R.id.user_mail);

            Button editBtn = (Button) toolbarLayout.findViewById(R.id.edit);
            userImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(context, EditAccountActivity.class));
                }
            });

            editBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(context, EditAccountActivity.class));
                }
            });*/
            toolbar.addView(toolbarLayout);
            toolbar.setVisibility(View.GONE);

            errorLayout.setVisibility(View.GONE);
            final List<String> list = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.profile_settings)));
            TypedArray listIcons = getResources().obtainTypedArray(R.array.profile_icons);
            ProfileSettingsAdapter adbPerson = new ProfileSettingsAdapter(context, list, listIcons);
            profileSettingLv.setAdapter(adbPerson);
            ListViewSizeHelper.getListViewSize(profileSettingLv);
            profileSettingLv.setOnItemClickListener((parent, view, position, id) -> openSettingPage(position));
            arrowImage.setTag(true);
//            collapse(listLayout);
            HomeActivity.updateNotificationCount(context, GlobalData.notificationCount);
            String VERSION_NAME = BuildConfig.VERSION_NAME;
            int versionCode = BuildConfig.VERSION_CODE;
            String appVersionText = "App version " + VERSION_NAME + " (" + String.valueOf(versionCode) + ")";
            appVersion.setText(appVersionText);
        } else {
            toolbar.setVisibility(View.GONE);
            //set Error Layout
            errorLayout.setVisibility(View.VISIBLE);
        }


    }

    private void changeLanguage() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getLayoutInflater();
        View convertView = inflater.inflate(R.layout.language_dialog, null);
        alertDialog.setView(convertView);
        alertDialog.setCancelable(true);
        alertDialog.setTitle(R.string.change_language);
        final AlertDialog alert = alertDialog.create();
        final RadioGroup chooseLanguage = convertView.findViewById(R.id.choose_language);
        final RadioButton english = convertView.findViewById(R.id.english);
        final RadioButton spanish = convertView.findViewById(R.id.german);

        String dd = LocaleUtils.getLanguage(context);
        switch (dd) {
            case "es":
                spanish.setChecked(true);
                break;
            case "en":
            default:
                english.setChecked(true);
                break;
        }
        chooseLanguage.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.english:
                    setLanguage("English");
                    alert.dismiss();
                    break;
                case R.id.german:
                    setLanguage("Spanish");
                    alert.dismiss();
                    break;

            }
        });
        alert.show();
    }

    private void setLanguage(String value) {
        SharedHelper.putKey(requireActivity(), "language", value);
        switch (value) {
            case "Spanish":
                LocaleUtils.setLocale(getActivity(), "es");
                break;
            case "English":
            default:
                LocaleUtils.setLocale(getActivity(), "en");
                break;
        }

        SharedHelper.putKey(context, "language", value);
        startActivity(new Intent(getActivity(), HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).putExtra("change_language", true));
        getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

    }

    private void initView() {

        if (GlobalData.profileModel != null) {
            Glide.with(context)
                    .load(GlobalData.profileModel.getAvatar())
                    .apply(new RequestOptions()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.man)
                            .error(R.drawable.man))
                    .into(userImage);
            //userPhone.setText(GlobalData.profileModel.getPhone());
            userName.setText(GlobalData.profileModel.getName());
            userEmail.setText(GlobalData.profileModel.getEmail());
        }
    }

    @OnClick({R.id.arrow_image, R.id.logout, R.id.myaccount_layout, R.id.login_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.myaccount_layout:
              /*  if (arrowImage.getTag().equals(true)) {
                    //rotate arrow image
                    arrowImage.animate().setDuration(500).rotation(180).start();
                    arrowImage.setTag(false);
                    //collapse animation
                    collapse(listLayout);
                    //viewLine.setVisibility(View.VISIBLE);

                } else {
                    //rotate arrow image
                    arrowImage.animate().setDuration(500).rotation(360).start();
                    arrowImage.setTag(true);
                    //viewLine.setVisibility(View.GONE);
                    //textLine.setVisibility(View.VISIBLE);
                    //expand animation
                    expand(listLayout);
                }*/
                startActivity(new Intent(context, EditAccountActivity.class));
                break;
            case R.id.logout:
                alertDialog();
                //LoginManager.getInstance().logOut();
                break;
            case R.id.login_btn:
                SharedHelper.putKey(context, "logged", "false");
                startActivity(new Intent(context, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                getActivity().finish();
                break;
        }
    }

    public static void expand(final View v) {
        v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        v.getLayoutParams().height = 0;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? ViewGroup.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }


    private void signOut() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                //taken from google api console (Web api client id)
                .requestIdToken(getString(R.string.google_client_id))
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mGoogleApiClient.connect();
        mGoogleApiClient.registerConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
            @Override
            public void onConnected(@Nullable Bundle bundle) {

//                FirebaseAuth.getInstance().signOut();
                if (mGoogleApiClient.isConnected()) {
                    Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
                        @Override
                        public void onResult(@NonNull Status status) {
                            if (status.isSuccess()) {
                                Log.d("MainAct", "Google User Logged out");
                               /* Intent intent = new Intent(LogoutActivity.this, LoginActivity.class);
                                startActivity(intent);
                                finish();*/
                            }
                        }
                    });
                }
            }

            @Override
            public void onConnectionSuspended(int i) {
                Log.d("MAin", "Google API Client Connection Suspended");
            }
        });
    }

    public void alertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(R.string.are_you_sure_logout)
                .setPositiveButton(getResources().getString(R.string.logout), (dialog, which) -> {
                    // continue with delete
                    if (SharedHelper.getKey(context, "login_by").equals("facebook"))
                        LoginManager.getInstance().logOut();
                    if (SharedHelper.getKey(context, "login_by").equals("google"))
                        signOut();
                    SharedHelper.putKey(context, "logged", "false");
                    startActivity(new Intent(context, WelcomeScreenActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    GlobalData.profileModel = null;
                    GlobalData.addCart = null;
                    GlobalData.notificationCount = 0;
                    getActivity().finish();

                })
                .setNegativeButton(getResources().getString(R.string.cancel), (dialog, which) -> {
                    // do nothing
                    dialog.dismiss();
                });

        AlertDialog alert = builder.create();
        alert.show();
        Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        nbutton.setTextColor(ContextCompat.getColor(context, R.color.theme));
        nbutton.setTypeface(nbutton.getTypeface(), Typeface.BOLD);
        Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setTextColor(ContextCompat.getColor(context, R.color.theme));
        pbutton.setTypeface(pbutton.getTypeface(), Typeface.BOLD);
    }

}
