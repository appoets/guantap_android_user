package com.copiaexigo.grocery.users.helper;

import android.content.Context;
import android.location.Location;

import androidx.annotation.NonNull;

import com.copiaexigo.grocery.users.build.api.ApiClient;
import com.copiaexigo.grocery.users.build.api.ApiInterface;
import com.copiaexigo.grocery.users.models.AddCart;
import com.copiaexigo.grocery.users.models.Address;
import com.copiaexigo.grocery.users.models.AddressList;
import com.copiaexigo.grocery.users.models.Card;
import com.copiaexigo.grocery.users.models.Cart;
import com.copiaexigo.grocery.users.models.CartAddon;
import com.copiaexigo.grocery.users.models.Category;
import com.copiaexigo.grocery.users.models.Cuisine;
import com.copiaexigo.grocery.users.models.DisputeMessage;
import com.copiaexigo.grocery.users.models.Order;
import com.copiaexigo.grocery.users.models.Otp;
import com.copiaexigo.grocery.users.models.Product;
import com.copiaexigo.grocery.users.models.Shop;
import com.copiaexigo.grocery.users.models.User;
import com.copiaexigo.grocery.users.models.specifiedshop.ProductItem;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Tamil on 9/22/2017.
 */

public class GlobalData {

    public static String hashcode = "";
    public Otp otpModel = null;

    public static double latitude;
    public static double longitude;
    public static String addressHeader = "";
    public static String zipCode = "";
    public static Location CURRENT_LOCATION = null;

    /*------------Filter--------*/
    public static boolean isPureVegApplied = false;
    public static boolean isOfferApplied = false;
    public static boolean shouldContinueService = false;
    public static ArrayList<Integer> cuisineIdArrayList = null;
    public static ArrayList<Integer> cuisineIdArrayListNew = null;
    public static ArrayList<Card> cardArrayList;
    public static boolean isCardChecked = false;
    public static String loginBy = "manual";
    public static String name;
    public static String email;
    public static String access_token;
    public static String mobileNumber;
    public static String imageUrl;
    public static String address = "";
    public static int addCartShopId = 0;
    public static User profileModel = null;
    public static Address selectedAddress = null;
    public static Order isSelectedOrder = null;
    public static Product isSelectedProduct = null;
    public static ProductItem isSelectedProductItem = null;
    public static Cart isSelctedCart = null;
    public static List<CartAddon> cartAddons = null;
    public static AddCart addCart = null;
    public static boolean isApplyFilter = false;


    public static List<Shop> shopList;
    public static List<Cuisine> cuisineList;
    public static List<Category> categoryList = null;
    public static List<Order> onGoingOrderList;
    public static List<DisputeMessage> disputeMessageList;
    public static List<Order> pastOrderList;
    public static AddressList addressList = null;
    public static List<String> ORDER_STATUS = Arrays.asList("ORDERED", "RECEIVED", "ASSIGNED", "PROCESSING", "REACHED", "PICKEDUP", "ARRIVED", "COMPLETED", "CANCELLED");

    public static Shop selectedShop;

    public static int selectedShopId = 0;
    public static DisputeMessage isSelectedDispute;

    public static int otpValue = 0;
    public static String mobilePluseCountryCode = "";
    public static String mobile = "";
    public static String countryCode = "";
    public static String currencySymbol = "$";
    public static int notificationCount = 0;

    //Search Fragment
    public static List<Shop> searchShopList;
    public static List<Product> searchProductList;

    public static ArrayList<HashMap<String, String>> foodCart;
    public static String accessToken = "";

    private static final GlobalData ourInstance = new GlobalData();

    public static GlobalData getInstance() {
        return ourInstance;
    }

    public static NumberFormat getNumberFormat() {
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.getDefault());
        numberFormat.setCurrency(Currency.getInstance("USD"));
        numberFormat.setMinimumFractionDigits(2);
        return numberFormat;
    }

    private GlobalData() {
    }

    public void getProfileWallet1(Context context, WalletReflect walletReflect) {
        String device_UDID = "";
        try {
            device_UDID = android.provider.Settings.Secure.getString(context.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
        } catch (Exception e) {
            device_UDID = "COULD NOT GET UDID";
            e.printStackTrace();
        }
        HashMap<String, String> map = new HashMap<>();
        map.put("device_type", "android");
        map.put("device_id", device_UDID);
        map.put("device_token", SharedHelper.getKey(context, "device_token"));
        Call<User> profileWallet = ApiClient.getRetrofit().create(ApiInterface.class).getProfile(map);
        profileWallet.enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
                if (response.isSuccessful()) {
                    GlobalData.profileModel.setWalletBalance(response.body().getWalletBalance());
                    walletReflect.ChangeWalletBalance();
                }
            }

            @Override
            public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {

            }
        });
    }
}
