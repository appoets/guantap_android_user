package com.copiaexigo.grocery.users.fragments;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.copiaexigo.grocery.users.R;
import com.copiaexigo.grocery.users.activities.OtherHelpActivity;
import com.copiaexigo.grocery.users.activities.SplashActivity;
import com.copiaexigo.grocery.users.adapter.DisputeMessageAdapter;
import com.copiaexigo.grocery.users.build.api.ApiClient;
import com.copiaexigo.grocery.users.build.api.ApiInterface;
import com.copiaexigo.grocery.users.helper.CustomDialog;
import com.copiaexigo.grocery.users.helper.GlobalData;
import com.copiaexigo.grocery.users.models.DisputeMessage;
import com.copiaexigo.grocery.users.models.Order;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.copiaexigo.grocery.users.helper.GlobalData.disputeMessageList;
import static com.copiaexigo.grocery.users.helper.GlobalData.isSelectedOrder;
import static com.copiaexigo.grocery.users.helper.GlobalData.profileModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderHelpFragment extends Fragment {

    private static final int REQUEST_PHONE_CALL = 1246;
    Unbinder unbinder;

    @BindView(R.id.help_rv)
    RecyclerView helpRv;
    @BindView(R.id.other_help_layout)
    LinearLayout otherHelpLayout;
    @BindView(R.id.call_us)
    Button callUs;
    @BindView(R.id.chat_us)
    Button chatUs;

    private Context context;
    private final Double priceAmount = 0.0;
    private final int DISPUTE_ID = 0;
    private final int itemQuantity = 0;
    private final String currency = "";
    private final Integer DISPUTE_HELP_ID = 0;
    private final ApiInterface apiInterface = ApiClient.getRetrofit().create(ApiInterface.class);
    private CustomDialog customDialog;
    private String disputeType;
    private Handler handler;

    public OrderHelpFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
    }


    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_order_help, container, false);
        unbinder = ButterKnife.bind(this, view);
        customDialog = new CustomDialog(context);
        getDisputeMessage();
        return view;
    }

    private void getDisputeMessage() {
        Call<List<DisputeMessage>> call = apiInterface.getDisputeList();
        call.enqueue(new Callback<List<DisputeMessage>>() {
            @Override
            public void onResponse(Call<List<DisputeMessage>> call, Response<List<DisputeMessage>> response) {
                if (response.isSuccessful()) {
                    Log.e("Dispute List : ", response.toString());
                    disputeMessageList = new ArrayList<>();
                    disputeMessageList.addAll(response.body());
                    updateDiputeLayout();
                } else {
                    updateDiputeLayout();
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().toString());
                        Toast.makeText(context, jObjError.optString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();

                    }
                }
            }

            @Override
            public void onFailure(Call<List<DisputeMessage>> call, Throwable t) {
                updateDiputeLayout();
                Toast.makeText(context, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateDiputeLayout() {
        if (disputeMessageList != null) {
            helpRv.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
            helpRv.setItemAnimator(new DefaultItemAnimator());
            helpRv.setHasFixedSize(true);
            DisputeMessageAdapter disputeMessageAdapter = new DisputeMessageAdapter(disputeMessageList, context, getActivity());
            helpRv.setAdapter(disputeMessageAdapter);
            if (disputeMessageList.size() > 0) {
                otherHelpLayout.setVisibility(View.GONE);
            } else {
                otherHelpLayout.setVisibility(View.VISIBLE);
            }
        } else {
            startActivity(new Intent(context, SplashActivity.class));
            getActivity().finish();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void showDialog() {
        final String[] disputeArrayList = {"COMPLAINED", "CANCELLED", "REFUND"};
        disputeType = "COMPLAINED";
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dispute_dialog, null);
        dialogBuilder.setView(dialogView);
        final EditText edt = (EditText) dialogView.findViewById(R.id.reason_edit);
        final Spinner disputeTypeSpinner = (Spinner) dialogView.findViewById(R.id.dispute_type);
        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, disputeArrayList);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        disputeTypeSpinner.setAdapter(arrayAdapter);
        disputeTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                disputeType = disputeArrayList[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        dialogBuilder.setTitle("ORDER #000" + isSelectedOrder.getId().toString());
        String reason = "OTHERS";
        dialogBuilder.setMessage(reason);
        dialogBuilder.setPositiveButton(R.string.submit, null);
        dialogBuilder.setNegativeButton(R.string.cancel, (dialog, whichButton) -> dialog.dismiss());
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.setOnShowListener(dialog -> {
            Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (edt.getText().toString().equalsIgnoreCase("")) {
                        Toast.makeText(context, R.string.please_enter_reason, Toast.LENGTH_SHORT).show();
                    } else {
                        dialog.dismiss();
                        HashMap<String, String> map = new HashMap<>();
                        map.put("order_id", GlobalData.isSelectedOrder.getId().toString());
                        map.put("status", "CREATED");
                        map.put("description", edt.getText().toString());
                        map.put("dispute_type", disputeType);
                        map.put("created_by", "user");
                        map.put("created_to", "user");
                        postDispute(map);
                    }
                }
            });
        });
        alertDialog.show();
    }

    private void postDispute(HashMap<String, String> map) {
        customDialog.show();
        Call<Order> call = apiInterface.postDispute(map);
        call.enqueue(new Callback<Order>() {
            @Override
            public void onResponse(@NonNull Call<Order> call, @NonNull Response<Order> response) {
                customDialog.dismiss();
                if (response.isSuccessful()) {
                    Toast.makeText(context, R.string.dispute_created_successfully, Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(context, jObjError.optString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Order> call, @NonNull Throwable t) {
                customDialog.dismiss();
                Toast.makeText(context, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @OnClick({R.id.chat_us, R.id.call_us, R.id.dispute_us})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.chat_us:
                startActivity(new Intent(getActivity(), OtherHelpActivity.class).putExtra("is_chat", true));
                break;
            case R.id.dispute_us:
                showDialog();
                break;
            case R.id.call_us: {
                goToCall();
                break;
            }
        }
    }

    private void goToCall() {
        if (profileModel.getAdminContactNo() != null && !profileModel.getAdminContactNo().isEmpty()) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + profileModel.getAdminContactNo()));
            startActivity(intent);
        } else {
            Toast.makeText(context, R.string.no_number_available, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PHONE_CALL) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                goToCall();
            } else {
                Toast.makeText(context, "Need permission", Toast.LENGTH_SHORT).show();
                ActivityCompat.requestPermissions(requireActivity(), new String[]{Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
            }
        }
    }
}
