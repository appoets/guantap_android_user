
package com.copiaexigo.grocery.users.models.addonsdata;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class AddonsList implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("shop_id")
    @Expose
    private Integer shopId;
    @SerializedName("deleted_at")
    @Expose
    private Object deletedAt;
    @SerializedName("addon_group_id")
    @Expose
    private Integer addonGroupId;
    @SerializedName("addonproducts")
    @Expose
    private Addonproducts addonproducts;

    private Boolean isItemCheck = false;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Object getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Object deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Integer getAddonGroupId() {
        return addonGroupId;
    }

    public void setAddonGroupId(Integer addonGroupId) {
        this.addonGroupId = addonGroupId;
    }

    public Addonproducts getAddonproducts() {
        return addonproducts;
    }

    public void setAddonproducts(Addonproducts addonproducts) {
        this.addonproducts = addonproducts;
    }

    public Boolean getItemCheck() {
        return isItemCheck;
    }

    public void setItemCheck(Boolean itemCheck) {
        isItemCheck = itemCheck;
    }
}
