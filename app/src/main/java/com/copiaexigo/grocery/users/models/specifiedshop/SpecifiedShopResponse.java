package com.copiaexigo.grocery.users.models.specifiedshop;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class SpecifiedShopResponse implements Serializable {

	@SerializedName("featured_products")
	private List<ProductItem> featuredProducts;

	@SerializedName("sale_products")
	private List<ProductItem> saleProducts;

	public List<ProductItem> getSaleProducts() {
		return saleProducts;
	}

	public void setSaleProducts(List<ProductItem> sale_products) {
		this.saleProducts = sale_products;
	}

	@SerializedName("categories")
	private List<CategoriesItem> categories;

	public void setFeaturedProducts(List<ProductItem> featuredProducts){
		this.featuredProducts = featuredProducts;
	}

	public List<ProductItem> getFeaturedProducts(){
		return featuredProducts;
	}

	public void setCategories(List<CategoriesItem> categories){
		this.categories = categories;
	}

	public List<CategoriesItem> getCategories(){
		return categories;
	}
}