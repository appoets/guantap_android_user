
package com.copiaexigo.grocery.users.models.addonsdata;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Addonproducts implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("addon_id")
    @Expose
    private Integer addonId;
    @SerializedName("product_id")
    @Expose
    private Integer productId;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("addon_group_id")
    @Expose
    private Integer addonGroupId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAddonId() {
        return addonId;
    }

    public void setAddonId(Integer addonId) {
        this.addonId = addonId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getAddonGroupId() {
        return addonGroupId;
    }

    public void setAddonGroupId(Integer addonGroupId) {
        this.addonGroupId = addonGroupId;
    }

}
