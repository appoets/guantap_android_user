package com.copiaexigo.grocery.users.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.copiaexigo.grocery.users.HomeActivity;
import com.copiaexigo.grocery.users.R;
import com.copiaexigo.grocery.users.adapter.ProductImageAdapter;
import com.copiaexigo.grocery.users.adapter.SizeAdapter;
import com.copiaexigo.grocery.users.build.api.ApiClient;
import com.copiaexigo.grocery.users.build.api.ApiInterface;
import com.copiaexigo.grocery.users.helper.CustomDialog;
import com.copiaexigo.grocery.users.helper.GlobalData;
import com.copiaexigo.grocery.users.models.AddCart;
import com.copiaexigo.grocery.users.models.ClearCart;
import com.copiaexigo.grocery.users.models.addonsdata.AddonGroups;
import com.copiaexigo.grocery.users.models.addonsdata.AddonsList;
import com.copiaexigo.grocery.users.models.specifiedshop.Prices;
import com.copiaexigo.grocery.users.models.specifiedshop.ProductItem;
import com.copiaexigo.grocery.users.utils.CircleIndicator;
import com.copiaexigo.grocery.users.utils.Utils;

import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.copiaexigo.grocery.users.helper.GlobalData.notificationCount;
import static com.copiaexigo.grocery.users.helper.GlobalData.profileModel;
import static com.copiaexigo.grocery.users.helper.GlobalData.selectedShop;
import static com.copiaexigo.grocery.users.helper.GlobalData.selectedShopId;

public class ProductFullDetailActivity extends AppCompatActivity implements
        ViewPager.OnPageChangeListener {

    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.indicator)
    CircleIndicator mIndicator;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvDescription)
    TextView tvDescription;
    @BindView(R.id.tvOfferPrice)
    TextView tvOfferPrice;
    @BindView(R.id.tvOriginalPrice)
    TextView tvOriginalPrice;
    @BindView(R.id.tvRegularPrice)
    TextView tvRegularPrice;
    @BindView(R.id.add_card_text_layout)
    TextView cardAddDetailLayout;
    @BindView(R.id.card_value)
    TextView cardTextValue;
    @BindView(R.id.rvAddons)
    RecyclerView rvAddons;

    private ProductItem product;
    private final ApiInterface apiInterface = ApiClient.getRetrofit().create(ApiInterface.class);
    private AddCart addCart;
    private ProductItem productItem;
    private SizeAdapter sizeAdapter;
    private int itemCount = 1;
    private final NumberFormat numberFormat = GlobalData.getNumberFormat();
    private CustomDialog customDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_details_activity);
        ButterKnife.bind(this);
        initIntentData();
    }


    private void initIntentData() {
        customDialog = new CustomDialog(this);
        sizeAdapter = new SizeAdapter();
        LinearLayoutManager manager = new LinearLayoutManager(this);
        rvAddons.setLayoutManager(manager);
        rvAddons.setHasFixedSize(false);
        rvAddons.setAdapter(sizeAdapter);
        Integer productId = getIntent().getIntExtra("productItemId", -1);
        getProductDetails(productId);
    }

    private void getProductDetails(Integer productId) {
        customDialog.show();
        Call<ProductItem> call = apiInterface.productDetails(productId);
        call.enqueue(new Callback<ProductItem>() {
            @Override
            public void onResponse(Call<ProductItem> call, Response<ProductItem> response) {
                customDialog.dismiss();
                if (!response.isSuccessful() && response.errorBody() != null) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(ProductFullDetailActivity.this, jObjError.optString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(ProductFullDetailActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else if (response.isSuccessful()) {
                    productItem = response.body();
                    viewPager.setAdapter(new ProductImageAdapter(productItem.getImages(), ProductFullDetailActivity.this));
                    viewPager.setCurrentItem(0);
                    viewPager.addOnPageChangeListener(ProductFullDetailActivity.this);
                    mIndicator.setViewPager(viewPager);
                    tvTitle.setText(productItem.getName());
                    tvDescription.setText(productItem.getDescription());
                    Prices price = productItem.getPrices();
                    tvOriginalPrice.setText(numberFormat.format(price.getOrignalPrice()));
                    double itemPrice = price.getOrignalPrice() - price.getPrice();
                    tvOfferPrice.setText(numberFormat.format(itemPrice));
                    tvRegularPrice.setText(numberFormat.format(price.getPrice()));
                    sizeAdapter.setItemList(productItem.getAddonGroups());
                }
            }

            @Override
            public void onFailure(Call<ProductItem> call, Throwable t) {
                customDialog.dismiss();
                Toast.makeText(ProductFullDetailActivity.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @OnClick({R.id.card_add_btn, R.id.card_minus_btn, R.id.add_card_text_layout, R.id.close_img, R.id.backPress})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.add_card_text_layout:
                addCartItem();
                break;
            case R.id.close_img:
            case R.id.backPress:
                finish();
                break;
            case R.id.card_add_btn:
                int countValue = Integer.parseInt(cardTextValue.getText().toString()) + 1;
                itemCount = itemCount + 1;
                cardTextValue.setText(String.valueOf(countValue));
                break;
            case R.id.card_minus_btn:
                if (!cardTextValue.getText().toString().equalsIgnoreCase("1")) {
                    int countMinusValue = Integer.parseInt(cardTextValue.getText().toString()) - 1;
                    itemCount = itemCount - 1;
                    cardTextValue.setText(String.valueOf(countMinusValue));
                }
                break;
        }
    }


    public void addCart(HashMap<String, String> map) {
        customDialog.show();
        List<AddonGroups> list = sizeAdapter.getItemList();
        for (AddonGroups addonGroups : list) {
            List<AddonsList> addonsLists = addonGroups.getAddon();
            for (AddonsList addon : addonsLists) {
                if (addon.getItemCheck()) {
                    map.put("product_addons[" + addon.getAddonproducts().getId() + "]", addon.getAddonproducts().getId().toString());
                    map.put("addons_qty[" + addon.getAddonproducts().getId() + "]", map.get("quantity"));
                    map.put("addons_price[" + addon.getAddonproducts().getId() + "]", "" + addon.getAddonproducts().getPrice());
                }
            }
        }
        Call<AddCart> call = apiInterface.postAddCart(map);
        call.enqueue(new Callback<AddCart>() {
            @Override
            public void onResponse(Call<AddCart> call, Response<AddCart> response) {
                customDialog.dismiss();
                if (!response.isSuccessful() && response.errorBody() != null) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(ProductFullDetailActivity.this, jObjError.optString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(ProductFullDetailActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else if (response.isSuccessful()) {
                    GlobalData.addCartShopId = selectedShopId;
                    addCart = response.body();
                    GlobalData.addCart = response.body();

                    int itemQuantity = 0;
                    int itemCount = 0;
                    if (addCart != null) {
                        itemCount = addCart.getProductList().size();
                    }

                    for (int i = 0; i < itemCount; i++) {
                        itemQuantity = itemQuantity + addCart.getProductList().get(i).getQuantity();
                    }

                    notificationCount = itemQuantity;
                    HomeActivity.updateNotificationCount(ProductFullDetailActivity.this, notificationCount);
                    startActivity(new Intent(ProductFullDetailActivity.this, ViewCartActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    overridePendingTransition(R.anim.slide_in_right, R.anim.anim_nothing);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<AddCart> call, Throwable t) {
                customDialog.dismiss();
                Toast.makeText(ProductFullDetailActivity.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void clearCart() {
        Call<ClearCart> call = apiInterface.clearCart();
        call.enqueue(new Callback<ClearCart>() {
            @Override
            public void onResponse(Call<ClearCart> call, Response<ClearCart> response) {
                if (!response.isSuccessful() && response.errorBody() != null) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(ProductFullDetailActivity.this, jObjError.optString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(ProductFullDetailActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else if (response.isSuccessful()) {
                    selectedShop = HotelViewActivity.shops;
                    GlobalData.addCart.getProductList().clear();
                    GlobalData.notificationCount = 0;
                    HashMap<String, String> map = new HashMap<>();
                    map.put("product_id", product.getId().toString());
                    map.put("quantity", "1");
                    Log.e("AddCart_Text", map.toString());
                    addCart(map);
                }
            }

            @Override
            public void onFailure(Call<ClearCart> call, Throwable t) {
                Toast.makeText(ProductFullDetailActivity.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                GlobalData.addCartShopId = selectedShop.getId();
            }
        });

    }

    private void addCartItem() {
        product = productItem;
        selectedShopId = productItem.getShopId();
        if (profileModel != null) {
            if (Utils.isShopChanged(product.getShopId())) {
                String message = String.format(getResources().getString(R.string.reorder_confirm_message), product.getName(), GlobalData.addCart.getProductList().get(0).getProduct().getShop().getName());
                AlertDialog.Builder builder = new AlertDialog.Builder(ProductFullDetailActivity.this);
                builder.setTitle(getResources().getString(R.string.replace_cart_item))
                        .setMessage(message)
                        .setPositiveButton(getResources().getString(R.string.yes), (dialog, which) -> {
                            // continue with delete
                            clearCart();
                        })
                        .setNegativeButton(getResources().getString(R.string.no), (dialog, which) -> {
                            // do nothing
                            dialog.dismiss();

                        });
                AlertDialog alert = builder.create();
                alert.show();
                Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
                nbutton.setTextColor(ContextCompat.getColor(this, R.color.theme));
                nbutton.setTypeface(nbutton.getTypeface(), Typeface.BOLD);
                Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
                pbutton.setTextColor(ContextCompat.getColor(this, R.color.theme));
                pbutton.setTypeface(pbutton.getTypeface(), Typeface.BOLD);
            } else {
                HashMap<String, String> map = new HashMap<>();
                map.put("product_id", product.getId().toString());
                map.put("quantity", cardTextValue.getText().toString());
                addCart(map);
            }
        } else {
            startActivity(new Intent(this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            overridePendingTransition(R.anim.slide_in_left, R.anim.anim_nothing);
            finish();
            Toast.makeText(this, getResources().getString(R.string.please_login_and_order_dishes), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onPageSelected(int i) {

    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }
}
