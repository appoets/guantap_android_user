package com.copiaexigo.grocery.users.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.copiaexigo.grocery.users.R;
import com.copiaexigo.grocery.users.activities.HotelViewActivity;
import com.copiaexigo.grocery.users.models.CategoriesItem;
import com.copiaexigo.grocery.users.utils.roundimage.RoundedImageView;

import java.util.List;

/**
 * Created by santhosh@appoets.com on 22-08-2017.
 */

public class HomeCategoryAdapter extends RecyclerView.Adapter<HomeCategoryAdapter.MyViewHolder> {
    private static ClickListener clickListener;
    private final List<CategoriesItem> list;
    private final Context context;
    private final int itemLayout;

    public HomeCategoryAdapter(List<CategoriesItem> list, Context con, int itemLayout) {
        this.list = list;
        context = con;
        this.itemLayout = itemLayout;
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        HomeCategoryAdapter.clickListener = clickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        CategoriesItem obj = list.get(position);
        holder.title.setText(obj.getName());

        if (!obj.getImages().isEmpty()) {
            Glide.with(context)
                    .load(obj.getImages().get(0).getUrl())
                    .apply(new RequestOptions()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.ic_leader_board)
                            .error(R.drawable.ic_leader_board))
                    .into(holder.bg);
        }


        holder.bg.setOnClickListener(view ->
                context.startActivity(new Intent(context, HotelViewActivity.class)
                        .putExtra("categoryTitle", holder.title.getText().toString())
                        .putExtra("categoryPosition", position + 1)
                        // .putExtra("categoryImage", obj.getImages().get(0).getUrl())
                        .putExtra("isCategory", true)));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface ClickListener {
        void onItemClick(int position, View v);

    }

    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public RoundedImageView bg;
        public TextView title;

        public MyViewHolder(View view) {
            super(view);
            bg = view.findViewById(R.id.icon);
            title = view.findViewById(R.id.title);
        }

        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }


}
