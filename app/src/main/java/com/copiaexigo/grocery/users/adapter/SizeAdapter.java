package com.copiaexigo.grocery.users.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.copiaexigo.grocery.users.R;
import com.copiaexigo.grocery.users.helper.GlobalData;
import com.copiaexigo.grocery.users.models.addonsdata.AddonGroups;
import com.copiaexigo.grocery.users.models.addonsdata.Addonproducts;
import com.copiaexigo.grocery.users.models.addonsdata.AddonsList;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class SizeAdapter extends SectionedRecyclerViewAdapter<SizeAdapter.SizeVH> {

    List<AddonGroups> itemList = new ArrayList<>();
    private final NumberFormat numberFormat = GlobalData.getNumberFormat();

    public SizeAdapter() {
    }

    @Override
    public int getSectionCount() {
        return itemList.size();
    }

    @Override
    public int getItemCount(int section) {
        return itemList.get(section).getAddon().size();
    }

    @Override
    public void onBindHeaderViewHolder(SizeVH sizeVH, int position) {
        sizeVH.bindHeaderData(itemList.get(position));
    }

    @Override
    public void onBindViewHolder(SizeVH sizeVH, final int section, final int relativePosition, int absolutePosition) {
        final AddonsList object = itemList.get(section).getAddon().get(relativePosition);
        sizeVH.bindAddonsData(object);
        sizeVH.titleCheck.setOnClickListener(v -> {
            itemList.get(section).getAddon().get(relativePosition).setItemCheck(!object.getItemCheck());
            notifyDataSetChanged();
        });
    }


    @NonNull
    @Override
    public SizeAdapter.SizeVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        switch (viewType) {
            case VIEW_TYPE_HEADER:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.addon_header_item, parent, false);
                return new SizeVH(v, true);
            case VIEW_TYPE_ITEM:
            default:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_on_items, parent, false);
                return new SizeVH(v, false);
        }
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    public List<AddonGroups> getItemList() {
        return itemList;
    }

    public void setItemList(List<AddonGroups> items) {
        this.itemList = items;
        notifyDataSetChanged();
    }

    class SizeVH extends RecyclerView.ViewHolder {
        private TextView txtHeader;

        private CheckBox titleCheck;
        private TextView txtPrice;

        public SizeVH(@NonNull View itemView, boolean isHeader) {
            super(itemView);
            if (isHeader) {
                txtHeader = itemView.findViewById(R.id.txtHeader);
            } else {
                titleCheck = itemView.findViewById(R.id.checkTxtTitle);
                txtPrice = itemView.findViewById(R.id.txtPrice);
            }

        }

        public void bindHeaderData(AddonGroups item) {
            txtHeader.setText(item.getName());
        }

        public void bindAddonsData(AddonsList item) {
            Addonproducts addonproducts = item.getAddonproducts();
            titleCheck.setText(item.getName());
            txtPrice.setText(numberFormat.format(addonproducts.getPrice()));
            titleCheck.setChecked(item.getItemCheck());
        }
    }
}