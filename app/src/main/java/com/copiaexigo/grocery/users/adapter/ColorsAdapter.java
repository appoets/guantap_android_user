package com.copiaexigo.grocery.users.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.copiaexigo.grocery.users.R;

import java.util.ArrayList;
import java.util.List;

public class ColorsAdapter extends RecyclerView.Adapter<ColorsAdapter.ColorsVH> {

    List<String> itemList = new ArrayList<>();

    public ColorsAdapter() {
    }

    @NonNull
    @Override
    public ColorsAdapter.ColorsVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_on_items, parent, false);
        return new ColorsVH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ColorsAdapter.ColorsVH holder, int position) {
        String item = itemList.get(position);
        holder.bindData(item);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setItemList(List<String> items) {
        items.add("Red");
        items.add("Yellow");
        items.add("Green");
        items.add("Blue");
        this.itemList = items;
        notifyDataSetChanged();
    }

    static class ColorsVH extends RecyclerView.ViewHolder {

        private final CheckBox titleCheck;
        private final TextView txtPrice;

        public ColorsVH(@NonNull View itemView) {
            super(itemView);
            titleCheck = itemView.findViewById(R.id.checkTxtTitle);
            txtPrice = itemView.findViewById(R.id.txtPrice);
        }

        public void bindData(String item) {
            titleCheck.setText("Item 1");
            txtPrice.setText("+ $0.00");
        }
    }
}
