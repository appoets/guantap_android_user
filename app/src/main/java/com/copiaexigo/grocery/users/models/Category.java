
package com.copiaexigo.grocery.users.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Category {

    @SerializedName("shop_id")
    private Integer shopId;

    @SerializedName("parent_id")
    private Integer parentId;

    @SerializedName("name")
    private String name;

    @SerializedName("description")
    private String description;

    @SerializedName("id")
    private Integer id;

    @SerializedName("position")
    private Integer position;

    @SerializedName("status")
    private String status;

    @SerializedName("products")
    private List<Product> products;

    public void setShopId(Integer shopId){
        this.shopId = shopId;
    }

    public Integer getShopId(){
        return shopId;
    }

    public void setParentId(Integer parentId){
        this.parentId = parentId;
    }

    public Integer getParentId(){
        return parentId;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public String getDescription(){
        return description;
    }

    public void setId(Integer id){
        this.id = id;
    }

    public Integer getId(){
        return id;
    }

    public void setPosition(Integer position){
        this.position = position;
    }

    public Integer getPosition(){
        return position;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public String getStatus(){
        return status;
    }

    public void setProducts(List<Product> products){
        this.products = products;
    }

    public List<Product> getProducts(){
        return products;
    }
}
