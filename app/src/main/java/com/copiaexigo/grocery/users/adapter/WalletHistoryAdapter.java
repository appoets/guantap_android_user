package com.copiaexigo.grocery.users.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.copiaexigo.grocery.users.R;
import com.copiaexigo.grocery.users.helper.GlobalData;
import com.copiaexigo.grocery.users.models.WalletHistory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by santhosh@appoets.com on 22-08-2017.
 */

public class WalletHistoryAdapter extends RecyclerView.Adapter<WalletHistoryAdapter.MyViewHolder> {
    private final List<WalletHistory> list;

    public WalletHistoryAdapter(List<WalletHistory> list) {
        this.list = list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.wallet_history_list_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        WalletHistory walletHistory = list.get(position);
        holder.amountTxt.setText(GlobalData.getNumberFormat().format(walletHistory.getAmount()));
        holder.timeTxt.setText(getFormatTime(walletHistory.getCreatedAt()));
        holder.statusTxt.setText(walletHistory.getStatus());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final TextView amountTxt;
        private final TextView timeTxt;
        private final TextView statusTxt;

        private MyViewHolder(View view) {
            super(view);
            amountTxt = view.findViewById(R.id.amount_txt);
            timeTxt = view.findViewById(R.id.time_txt);
            statusTxt = view.findViewById(R.id.status_txt);
        }
    }

    private String getFormatTime(String time) {
        System.out.println("Time : " + time);
        String value = "";
        try {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("EEE, MMM d, hh:mm aaa");
            if (time != null) {
                Date date = df.parse(time);
                value = sdf.format(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return value;
    }
}
