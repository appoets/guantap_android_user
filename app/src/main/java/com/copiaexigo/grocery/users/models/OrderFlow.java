package com.copiaexigo.grocery.users.models;

/**
 * Created by Tamil on 10/9/2017.
 */

public class OrderFlow {
    public   int statusImage;
    public  String statusTitle,statusDescription,status;
    public   boolean active = false;

    public  OrderFlow(String statusTitle,String statusDescription, int statusImage,String status){
        this.statusImage=statusImage;
        this.statusTitle=statusTitle;
        this.statusDescription=statusDescription;
        this.status=status;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
