package com.copiaexigo.grocery.users.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.copiaexigo.grocery.users.R;
import com.copiaexigo.grocery.users.fragments.OrderViewFragment;
import com.copiaexigo.grocery.users.helper.GlobalData;
import com.copiaexigo.grocery.users.models.Order;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.copiaexigo.grocery.users.helper.GlobalData.isSelectedOrder;

//import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PastOrderDetailActivity extends AppCompatActivity {

    @BindView(R.id.order_id_txt)
    TextView orderIdTxt;
    @BindView(R.id.order_item_txt)
    TextView orderItemTxt;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.source_image)
    ImageView sourceImage;
    @BindView(R.id.restaurant_name)
    TextView restaurantName;
    @BindView(R.id.restaurant_address)
    TextView restaurantAddress;
    @BindView(R.id.source_layout)
    RelativeLayout sourceLayout;
    @BindView(R.id.destination_image)
    ImageView destinationImage;
    @BindView(R.id.user_address_title)
    TextView userAddressTitle;
    @BindView(R.id.user_address)
    TextView userAddress;
    @BindView(R.id.destination_layout)
    RelativeLayout destinationLayout;
    @BindView(R.id.view_line2)
    View viewLine2;
    @BindView(R.id.order_succeess_image)
    ImageView orderSucceessImage;
    @BindView(R.id.order_status_txt)
    TextView orderStatusTxt;
    @BindView(R.id.order_status_layout)
    RelativeLayout orderStatusLayout;
    @BindView(R.id.order_detail_fargment)
    FrameLayout orderDetailFargment;
    @BindView(R.id.nested_scroll_view)
    NestedScrollView nestedScrollView;
    @BindView(R.id.dot_line_img)
    ImageView dotLineImg;


    private final NumberFormat numberFormat = GlobalData.getNumberFormat();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        ButterKnife.bind(this);
        //Toolbar
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
        if (isSelectedOrder != null) {
            Order order = GlobalData.isSelectedOrder;
            orderIdTxt.setText(getString(R.string.order_idss) + order.getId().toString());
            int itemQuantity = order.getInvoice().getQuantity();
            Double priceAmount = order.getInvoice().getPayable();
            if (order.getStatus().equalsIgnoreCase("CANCELLED")) {
                orderStatusTxt.setText(getResources().getString(R.string.order_cancelled));
                orderSucceessImage.setImageResource(R.drawable.order_cancelled_img);
                dotLineImg.setBackgroundResource(R.drawable.order_cancelled_line);
                orderStatusTxt.setTextColor(ContextCompat.getColor(this, R.color.colorRed));
            } else {
                orderStatusTxt.setText(getResources().getString(R.string.order_delivered_successfully_on) + getFormatTime(order.getOrdertiming().get(7).getCreatedAt()));
                orderStatusTxt.setTextColor(ContextCompat.getColor(this, R.color.colorGreen));
                orderSucceessImage.setImageResource(R.drawable.ic_circle_tick);
                dotLineImg.setBackgroundResource(R.drawable.ic_line);
            }

            if (itemQuantity == 1)
                orderItemTxt.setText(itemQuantity + getString(R.string.item_) + numberFormat.format(priceAmount));
            else
                orderItemTxt.setText(itemQuantity + getString(R.string.items_) + numberFormat.format(priceAmount));

            restaurantName.setText(order.getShop().getName());
            restaurantAddress.setText(order.getShop().getAddress());
            userAddressTitle.setText(order.getAddress().getType());
            userAddress.setText(order.getAddress().getMapAddress());
            //set Fragment
            Fragment orderFullViewFragment = new OrderViewFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.add(R.id.order_detail_fargment, orderFullViewFragment).commit();
        }
    }

    private String getFormatTime(String time) {
        System.out.println("Time : " + time);
        String value = "";
        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm aa", Locale.getDefault());
            if (time != null) {
                Date date = df.parse(time);
                value = sdf.format(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();

        }
        return value;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.anim_nothing, R.anim.slide_out_right);
    }
    /*@Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }*/
}