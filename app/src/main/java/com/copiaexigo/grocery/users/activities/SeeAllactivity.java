package com.copiaexigo.grocery.users.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.copiaexigo.grocery.users.R;
import com.copiaexigo.grocery.users.adapter.BannerAdapter;
import com.copiaexigo.grocery.users.adapter.HomeCategoryAdapter;
import com.copiaexigo.grocery.users.adapter.ProductAdapter;
import com.copiaexigo.grocery.users.adapter.RestaurantsAdapter;
import com.copiaexigo.grocery.users.build.api.ApiClient;
import com.copiaexigo.grocery.users.build.api.ApiInterface;
import com.copiaexigo.grocery.users.helper.GlobalData;
import com.copiaexigo.grocery.users.models.CategoriesItem;
import com.copiaexigo.grocery.users.models.CategoryPackages;
import com.copiaexigo.grocery.users.models.HomeCategoryResponse;
import com.copiaexigo.grocery.users.models.Shop;
import com.copiaexigo.grocery.users.models.offers.OffersModel;
import com.copiaexigo.grocery.users.models.offers.Product;
import com.copiaexigo.grocery.users.models.specifiedshop.ProductItem;
import com.copiaexigo.grocery.users.models.specifiedshop.SpecifiedShopResponse;
import com.ethanhua.skeleton.Skeleton;
import com.ethanhua.skeleton.SkeletonScreen;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.copiaexigo.grocery.users.helper.GlobalData.latitude;
import static com.copiaexigo.grocery.users.helper.GlobalData.longitude;

public class SeeAllactivity extends AppCompatActivity {

    @BindView(R.id.tv_allItemTitle)
    TextView tvTitle;
    @BindView(R.id.rvAllItems)
    RecyclerView rvAllItems;
    @BindView(R.id.back)
    ImageView back;

    private BannerAdapter bannerAdapter;
    private final List<Product> bannerList = new ArrayList<>();
    private ProductAdapter adapterProduct;

    //shops see All
    private final List<ProductItem> featuredProductItems = new ArrayList<>();
    private final List<ProductItem> salesProductItems = new ArrayList<>();
    private SkeletonScreen skeletonText2;
    private final ApiInterface apiInterface = ApiClient.getRetrofit().create(ApiInterface.class);

    //for All Categories
    private HomeCategoryAdapter adapterDiscover;
    private final List<CategoriesItem> discoverList = new ArrayList<>();
    private final List<Shop> restaurantList = new ArrayList<>();
    private String seeAllCallfrom;
    private Boolean isCategory;
    private int categoryPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_allactivity);
        ButterKnife.bind(this);
        isCategory = getIntent().getBooleanExtra("IS_CATEGORY", false);
        categoryPosition = getIntent().getIntExtra("IS_CATEGORY_POSITION", 0);
        if (getIntent().hasExtra("CALL_FROM")) {
            if (getIntent().getStringExtra("CALL_FROM") != null) {
                seeAllCallfrom = getIntent().getStringExtra("CALL_FROM");
            }
        }

        SkeletonScreen skeletonScreen2 = Skeleton.bind(rvAllItems)
                .adapter(bannerAdapter)
                .load(R.layout.skeleton_impressive_list_item_verticle)
                .count(3)
                .show();
        skeletonText2 = Skeleton.bind(tvTitle)
                .load(R.layout.skeleton_label)
                .show();

        skeletonScreen2.show();
        skeletonText2.show();

        switch (seeAllCallfrom) {
            case "CATEGORIES":
                tvTitle.setText(this.getString(R.string.categories));
                getCategoryList();
                break;
            case "DEALS":
                tvTitle.setText(this.getString(R.string.deals_amp_offers));
                getOffers();
                break;
            case "SHOPS":
                tvTitle.setText(this.getString(R.string.shops_near_you));
                getRestaurant();
                break;
            case "SALES":
                tvTitle.setText(this.getString(R.string.sales));
                if (isCategory) {
                    getCategoryData();
                } else {
                    getRestaurantByShopId();
                }
                break;
            case "FEATURED":
                tvTitle.setText(this.getString(R.string.featured_products));
                getRestaurantByShopId();
                break;
            default:
                onBackPressed();
                break;
        }
        setUpRecylerView();

        back.setOnClickListener(v -> onBackPressed());

    }

    private void setUpRecylerView() {
        rvAllItems.setHasFixedSize(true);
        rvAllItems.setItemViewCacheSize(20);
        rvAllItems.setDrawingCacheEnabled(true);
        rvAllItems.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        rvAllItems.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvAllItems.setItemAnimator(new DefaultItemAnimator());
    }

    public void getOffers() {
        skeletonText2.hide();
        Call<OffersModel> call = apiInterface.getHomeOfferList();
        call.enqueue(new Callback<OffersModel>() {
            @Override
            public void onResponse(Call<OffersModel> call, Response<OffersModel> response) {
                if (response != null && !response.isSuccessful() && response.errorBody() != null) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(SeeAllactivity.this, jObjError.optString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(SeeAllactivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else if (response.isSuccessful()) {
                    if (response.body().getProducts().size() == 0) {
                        tvTitle.setVisibility(View.GONE);
                    } else {
                        tvTitle.setVisibility(View.VISIBLE);
                    }
                    bannerList.clear();
                    bannerList.addAll(response.body().getProducts());
                    bannerAdapter = new BannerAdapter(bannerList, SeeAllactivity.this, SeeAllactivity.this, R.layout.seeall_list_item);
                    rvAllItems.setAdapter(bannerAdapter);
                }
            }

            @Override
            public void onFailure(Call<OffersModel> call, Throwable t) {

            }
        });

    }

    public void getCategoryList() {
        skeletonText2.hide();
        Call<HomeCategoryResponse> call = apiInterface.getHomeCategoryList();
        call.enqueue(new Callback<HomeCategoryResponse>() {
            @Override
            public void onResponse(Call<HomeCategoryResponse> call, Response<HomeCategoryResponse> response) {
                if (response != null && !response.isSuccessful() && response.errorBody() != null) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(SeeAllactivity.this, jObjError.optString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(SeeAllactivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else if (response.isSuccessful()) {
                    //Check Banner list
                    if (response.body().getCategories().size() == 0) {
                        tvTitle.setVisibility(View.GONE);
                    } else {
                        tvTitle.setVisibility(View.VISIBLE);
                    }
                    discoverList.clear();
                    discoverList.addAll(response.body().getCategories());
                    adapterDiscover = new HomeCategoryAdapter(discoverList, SeeAllactivity.this, R.layout.home_category_all_list_item);
                    rvAllItems.setAdapter(adapterDiscover);
                }
            }

            @Override
            public void onFailure(Call<HomeCategoryResponse> call, Throwable t) {

            }
        });
    }


    public void getCategoryData() {
        skeletonText2.hide();
        HashMap<String, String> map = new HashMap<>();
        if (GlobalData.profileModel != null) {
            map.put("user_id", String.valueOf(GlobalData.profileModel.getId()));
        }
        Call<CategoryPackages> call = apiInterface.getParticularcCategoryList(categoryPosition, map);
        call.enqueue(new Callback<CategoryPackages>() {
            @Override
            public void onResponse(Call<CategoryPackages> call, Response<CategoryPackages> response) {
//                Log.i("onResponse: ",response.body().toString());
                if (response.isSuccessful()) {
                    if (response.body() != null && response.body().getProducts() != null) {
                        salesProductItems.clear();
                        salesProductItems.addAll(response.body().getProducts());
                        adapterProduct = new ProductAdapter(salesProductItems, SeeAllactivity.this, R.layout.accompainment_all_list_item);
                        GridLayoutManager manager = new GridLayoutManager(SeeAllactivity.this, 2, GridLayoutManager.VERTICAL, false);
                        rvAllItems.setLayoutManager(manager);
                        rvAllItems.setAdapter(adapterProduct);
                    }
                } else
                    Toast.makeText(SeeAllactivity.this, R.string.no_list_in_categpry, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<CategoryPackages> call, Throwable t) {

            }
        });
    }

    private void getRestaurant() {
        skeletonText2.hide();
        restaurantList.clear();
        restaurantList.addAll(GlobalData.shopList);
        //for All Shops
        RestaurantsAdapter adapterRestaurant = new RestaurantsAdapter(restaurantList, SeeAllactivity.this, SeeAllactivity.this, R.layout.restaurant_all_list_item);
        rvAllItems.setAdapter(adapterRestaurant);
    }

    private void getRestaurantByShopId() {
        skeletonText2.hide();
        HashMap<String, String> map = new HashMap<>();
        map.put("latitude", String.valueOf(latitude));
        map.put("longitude", String.valueOf(longitude));
        Call<SpecifiedShopResponse> call = apiInterface.getShopById(GlobalData.selectedShop.getId(), map);
        call.enqueue(new Callback<SpecifiedShopResponse>() {
            @Override
            public void onResponse(Call<SpecifiedShopResponse> call, Response<SpecifiedShopResponse> response) {
                if (response != null && !response.isSuccessful() && response.errorBody() != null) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(SeeAllactivity.this, jObjError.optString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(SeeAllactivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else if (response.isSuccessful()) {
                    if (response.body().getCategories().size() == 0) {
                        tvTitle.setVisibility(View.GONE);
                    } else {
                        tvTitle.setVisibility(View.VISIBLE);
                    }
                    if (seeAllCallfrom.equals("FEATURED")) {
                        featuredProductItems.clear();
                        featuredProductItems.addAll(response.body().getFeaturedProducts());
                        adapterProduct = new ProductAdapter(featuredProductItems, SeeAllactivity.this, R.layout.accompainment_all_list_item);
                    }

                    if (response.body().getCategories().size() == 0) {
                        tvTitle.setVisibility(View.GONE);
                    } else {
                        tvTitle.setVisibility(View.VISIBLE);
                    }

                    if (seeAllCallfrom.equals("SALES")) {
                        salesProductItems.clear();
                        salesProductItems.addAll(response.body().getSaleProducts());
                        adapterProduct = new ProductAdapter(salesProductItems, SeeAllactivity.this, R.layout.accompainment_all_list_item);
                    }
                }

                GridLayoutManager manager = new GridLayoutManager(SeeAllactivity.this, 2, GridLayoutManager.VERTICAL, false);
                rvAllItems.setLayoutManager(manager);
                rvAllItems.setAdapter(adapterProduct);
            }

            @Override
            public void onFailure(Call<SpecifiedShopResponse> call, Throwable t) {
                Toast.makeText(SeeAllactivity.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
            }
        });
    }

}