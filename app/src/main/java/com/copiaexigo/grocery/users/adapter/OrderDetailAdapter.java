package com.copiaexigo.grocery.users.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.copiaexigo.grocery.users.R;
import com.copiaexigo.grocery.users.models.CartAddon;
import com.copiaexigo.grocery.users.models.Item;

import java.text.NumberFormat;
import java.util.List;

/**
 * Created by santhosh@appoets.com on 22-08-2017.
 */

public class OrderDetailAdapter extends RecyclerView.Adapter<OrderDetailAdapter.MyViewHolder> {
    private final List<Item> list;
    private final Context context;
    private final NumberFormat numberFormat;

    public OrderDetailAdapter(List<Item> list, Context con, NumberFormat noFormat) {
        this.list = list;
        this.context = con;
        this.numberFormat = noFormat;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_detail_list_item, parent, false);
        return new MyViewHolder(itemView);
    }

    public void add(Item item, int position) {
        list.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(Item item) {
        int position = list.indexOf(item);
        list.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Item item = list.get(position);
        holder.dishName.setText(item.getProduct().getName() + " x " + item.getQuantity());
        double priceAmount = item.getProduct().getPrices().getOrignalPrice() * item.getQuantity();
        holder.price.setText(numberFormat.format(priceAmount));

        if (item.getProduct().getFoodType().equalsIgnoreCase("veg"))
            holder.dishImg.setImageResource(R.drawable.ic_veg);
        else
            holder.dishImg.setImageResource(R.drawable.ic_nonveg);

        if (item.getCartAddons() != null && !item.getCartAddons().isEmpty()) {
            List<CartAddon> cartAddonList = item.getCartAddons();
            holder.layoutAddons.removeAllViews();
            for (int i = 0; i < cartAddonList.size(); i++) {
                if (cartAddonList.get(i).getAddonProduct() != null) {
                    View itemView = LayoutInflater.from(holder.itemView.getContext()).inflate(R.layout.list_addon, null, false);
                    TextView addonName = itemView.findViewById(R.id.addon_name);
                    TextView addonPrice = itemView.findViewById(R.id.addon_price);
                    CartAddon object = cartAddonList.get(i);
                    String value = itemView.getContext().getResources().getString(R.string.addon_,
                            object.getAddonProduct().getAddon().getName(),
                            object.getQuantity(),
                            numberFormat.format(object.getAddonProduct().getPrice()));
                    addonName.setText(value);
                    Double totalAmount = Double.valueOf(object.getAddonProduct().getPrice() * object.getQuantity());
                    addonPrice.setText(numberFormat.format(totalAmount));
                    holder.layoutAddons.addView(itemView);
                }
            }

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final LinearLayout itemView;
        private final ImageView dishImg;
        private final TextView dishName;
        private final TextView price;
        private final LinearLayout layoutAddons;

        private MyViewHolder(View view) {
            super(view);
            itemView = view.findViewById(R.id.item_view);
            dishName = view.findViewById(R.id.dish_name);
            dishImg = view.findViewById(R.id.food_type_image);
            price = view.findViewById(R.id.price);
            layoutAddons = view.findViewById(R.id.layoutAddons);
        }
    }
}
